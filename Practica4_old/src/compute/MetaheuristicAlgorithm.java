package compute;

import java.util.HashSet;
import java.util.Set;

import reading.FileRead;
import bean.Solution;

public class MetaheuristicAlgorithm
{
	private ConstructiveAlgorithm ca;
	private LocalSearchAlgorithm lsa;
	private final FileRead fileRead;
	private Set<Integer> solutionPoints;
	private Solution solution;
	

	public MetaheuristicAlgorithm(ConstructiveAlgorithm constructiveAlgorithm, LocalSearchAlgorithm localSearchAlgorithm) 
	{
		this(constructiveAlgorithm,
			localSearchAlgorithm,
			localSearchAlgorithm.getFileRead(),
			localSearchAlgorithm.getSolutionPoints(), 
			localSearchAlgorithm.getSolution());	
	}
	
	public MetaheuristicAlgorithm(ConstructiveAlgorithm constructiveAlgorithm, LocalSearchAlgorithm localSearchAlgorithm, FileRead fileRead, Set<Integer> solutionPoints, Solution solution) 
	{
		this.ca = constructiveAlgorithm;
		this.lsa = localSearchAlgorithm;
		this.fileRead = fileRead;
		this.solutionPoints = new HashSet<Integer>(solutionPoints);
		this.solution = new Solution(solution);
	}
	
	public void compute(Algorithm algorithm)
	{
		if(algorithm.name().equalsIgnoreCase(Algorithm.ITERATED_GREEDY.name()))
		{
			this.iterated_greedy();
		}
	}

	private void iterated_greedy() 
	{
		//TODO: Iterated Greedy Metaheuristic
	}
	
	public FileRead getFileRead() 
	{
		return this.fileRead;
	}

	public Set<Integer> getSolutionPoints() 
	{
		return this.solutionPoints;
	}

	public Solution getSolution() 
	{
		return this.solution;
	}
	
	public ConstructiveAlgorithm getCa() {
		return ca;
	}

	public void setCa(ConstructiveAlgorithm ca) {
		this.ca = ca;
	}

	public LocalSearchAlgorithm getLsa() {
		return lsa;
	}

	public void setLsa(LocalSearchAlgorithm lsa) {
		this.lsa = lsa;
	}

	public void setSolutionPoints(Set<Integer> solutionPoints) {
		this.solutionPoints = solutionPoints;
	}

	public void setSolution(Solution solution) {
		this.solution = solution;
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[fileRead=");
		sb.append(this.fileRead.getFileName() + ", " + System.lineSeparator());
		/*
		sb.append(this.fileRead.getFileName() + ", " + System.lineSeparator() + "solutionPoints=[");
		for(int i : this.solutionPoints)
		{
			sb.append(i + ", ");
		}
		//sb.insert(sb.capacity() - 2, "]");
		sb.append("]," + System.lineSeparator() + "solution=" + this.solution);
		sb.append("," + System.lineSeparator() + "Objetive Function= " + this.solution.minValue() + "]");
		*/
		sb.append("solution= " + this.solutionPoints);
		sb.append("," + System.lineSeparator());
		sb.append("Objetive Function= " + this.solution.minValue() + "]");
		return sb.toString();
	}

}
