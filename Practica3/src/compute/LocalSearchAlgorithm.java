package compute;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import reading.FileRead;
import bean.Point;
import bean.Solution;

/*
 * This class compute a solution from giving solution
 * through some local search heuristics (stipulated in the Algorithm class)
 */
public class LocalSearchAlgorithm 
{
	private final FileRead fileRead;
	private Set<Integer> solutionPoints;
	private Solution solution;

	
	public LocalSearchAlgorithm(ConstructiveAlgorithm constructiveAlgorithm) 
	{
		this(constructiveAlgorithm.getFileRead(),
			constructiveAlgorithm.getSolutionPoints(), 
			constructiveAlgorithm.getSolution());
	}
	
	public LocalSearchAlgorithm(FileRead fileRead, Set<Integer> solutionPoints, Solution solution) 
	{
		this.fileRead = fileRead;
		this.solutionPoints = new HashSet<Integer>(solutionPoints);
		this.solution = new Solution(solution);
	}
	
	//Compute Algorithm
	public void compute(Algorithm algorithm)
	{
		if(algorithm.name().equals(Algorithm.BEST_IMPROVEMENT.name()))
		{
			this.bestImprovement();
		}
		else if(algorithm.name().equalsIgnoreCase(Algorithm.FIRST_LEXICOGRAPHICAL_IMPROVEMENT.name()))
		{
			this.firstLexicographicalImprovement();
		}
		else
		{
			this.firstRandomImprovement();
		}
	}
	
	/*
	 * FIRST RANDOM IMPROVEMENT
	 */
	private void firstRandomImprovement() 
	{
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		Object[] oldSolutionPoints = this.solutionPoints.toArray();
		Set<Integer> selectedPoints = new HashSet<Integer>();
		int compare;
		boolean improve;
		Random rnd = new Random(this.fileRead.getChoosen());
		
		//Iterar hasta que no haya mejora
		do
		{
			improve = false;
			oldSolutionPoints = this.solutionPoints.toArray();
			selectedPoints.clear();
			int solutionPoint;
			
			for(int i = 0; i < oldSolutionPoints.length; i++)
			{
				do
				{
					solutionPoint = (int) oldSolutionPoints[rnd.nextInt(oldSolutionPoints.length)];
				}while(selectedPoints.contains(solutionPoint));
				selectedPoints.add(solutionPoint);
				
				Solution bestSolution = new Solution(this.solution);
				this.solution.removeXPoints(solutionPoint);
				this.solution.removeYPoints(solutionPoint);
				this.solutionPoints.remove(solutionPoint);			
				
				//Búsqueda de una solución mejor a la anterior
				//Se elimina un punto y se prueba el siguiente
				//Se escoge la mejor solución y se pasa a la siguiente iteración
				for(int j = 0; j < this.fileRead.getSize(); j++)
				{
					//Si se va a examinar el punto elegido o un punto que ya está
					//en la solución se salta a la siguiente iteración
					if(j == solutionPoint || this.solutionPoints.contains(j))
					{
						continue;
					}
					
					this.fillSolution(j, matrixDistances[j]);
					this.solutionPoints.add(j);
					compare = this.solution.compareTo(bestSolution);
					//Si se obtiene una mejor solución se acepta
					//y no se comprueban más posibilidades con la solución anterior
					if(compare == 1)
					{
						improve = true;
						bestSolution = new Solution(this.solution);
						break;
					}
					this.solution.removeXPoints(j);
					this.solution.removeYPoints(j);
					this.solutionPoints.remove(j);
				}
				if(improve)
				{
					break;
				}
				this.fillSolution(solutionPoint, matrixDistances[solutionPoint]);
				this.solutionPoints.add(solutionPoint);
			}
		}while(improve);
	}

	/*
	 * FIRST LEXICOGRAPHICAL IMPROVEMENT
	 */
	private void firstLexicographicalImprovement() 
	{
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		Set<Integer> oldSolutionPoints = new HashSet<Integer>(this.solutionPoints);
		int compare;
		boolean improve;
		
		//Iterar hasta que no haya mejora
		do
		{
			improve = false;
			oldSolutionPoints = new HashSet<Integer>(this.solutionPoints);
			for(int solutionPoint : oldSolutionPoints)
			{
				Solution bestSolution = new Solution(this.solution);
				this.solution.removeXPoints(solutionPoint);
				this.solution.removeYPoints(solutionPoint);
				this.solutionPoints.remove(solutionPoint);			
				
				//Búsqueda de una solución mejor a la anterior
				//Se elimina un punto y se prueba el siguiente
				//Se escoge la mejor solución y se pasa a la siguiente iteración
				for(int i = 0; i < this.fileRead.getSize(); i++)
				{
					if(i == solutionPoint || this.solutionPoints.contains(i))
					{
						continue;
					}
					
					this.fillSolution(i, matrixDistances[i]);
					this.solutionPoints.add(i);
					compare = this.solution.compareTo(bestSolution);
					if(compare == 1)
					{
						improve = true;
						bestSolution = new Solution(this.solution);
						break;
					}
					this.solution.removeXPoints(i);
					this.solution.removeYPoints(i);
					this.solutionPoints.remove(i);
				}
				if(improve)
				{
					break;
				}
				this.fillSolution(solutionPoint, matrixDistances[solutionPoint]);
				this.solutionPoints.add(solutionPoint);
			}
		}while(improve);
	}

	/*
	 * BEST IMPROVEMENT
	 */
	@SuppressWarnings("unchecked")
	private void bestImprovement() 
	{
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		Set<Integer> oldSolutionPoints;
		Random rnd = new Random(this.fileRead.getChoosen());
		
		//Iterar hasta que no haya mejora
		do
		{
			oldSolutionPoints = new HashSet<Integer>(this.solutionPoints);
			for(int solutionPoint : oldSolutionPoints)
			{
				Map<Solution, Set<Integer>> bestSolutions = new HashMap<Solution, Set<Integer>>();
				Solution bestSolution = new Solution(this.solution);
				bestSolutions.put(bestSolution, new HashSet<Integer>(this.solutionPoints));
				this.solution.removeXPoints(solutionPoint);
				this.solution.removeYPoints(solutionPoint);
				this.solutionPoints.remove(solutionPoint);
				boolean improve = false;
				int compare;
				
				//Búsqueda de una solución mejor a la anterior
				//Se elimina un punto y se prueba el siguiente
				//Se guardan todas las posibles soluciones en el Map: bestSolutions
				for(int i = 0; i < this.fileRead.getSize(); i++)
				{
					if(i == solutionPoint || this.solutionPoints.contains(i))
					{
						continue;
					}
					
					this.fillSolution(i, matrixDistances[i]);
					this.solutionPoints.add(i);
					compare = this.solution.compareTo(bestSolution);
					if(compare == 1)
					{
						improve = true;
						bestSolutions.clear();
						bestSolution = new Solution(this.solution);
						bestSolutions.put(bestSolution, new HashSet<Integer>(this.solutionPoints));
					}
					else if(compare == 0)
					{
						bestSolutions.put(new Solution(this.solution), new HashSet<Integer>(this.solutionPoints));
					}
					this.solution.removeXPoints(i);
					this.solution.removeYPoints(i);
					this.solutionPoints.remove(i);
				}
				this.fillSolution(solutionPoint, matrixDistances[solutionPoint]);
				this.solutionPoints.add(solutionPoint);
				
				//Si hay varias "best solutions" se escoge una al azar
				//siempre que se haya mejorado la solución anterior
				if(bestSolutions.size() > 1 && improve)
				{
					int aux = rnd.nextInt(bestSolutions.size());
					this.solution = (Solution) bestSolutions.keySet().toArray()[aux];
					this.solutionPoints = (Set<Integer>) bestSolutions.values().toArray()[aux];
				}
			}
		}while(! oldSolutionPoints.containsAll(this.solutionPoints));
	}

	private void fillSolution(int pos_x, float[] vectorDistances)
	{
		List<Point> points = this.solution.getPoints();
		for(int i : this.solutionPoints)
		{
			points.add(new Point(pos_x, i, vectorDistances[i]));
		}
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[fileRead="); 
		sb.append(this.fileRead.getFileName() + ", " + System.lineSeparator() + "solutionPoints=[");
		for(int i : this.solutionPoints)
		{
			sb.append(i + ", ");
		}
		//sb.insert(sb.capacity() - 2, "]");
		sb.append("]," + System.lineSeparator() + "solution=" + this.solution);
		sb.append("," + System.lineSeparator() + "Objetive Function= " + this.solution.minValue() + "]");
		return sb.toString();
	}

}
