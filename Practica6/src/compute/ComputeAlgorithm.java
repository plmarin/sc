package compute;

import solution.Solution;

public interface ComputeAlgorithm 
{
	public void compute(Algorithm algorithm);
	
	public Solution getSolution();
}
