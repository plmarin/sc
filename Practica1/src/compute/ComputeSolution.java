package compute;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import reading.FileRead;
import bean.Point;
import bean.Solution;

/*
 * This class compute a solution through some constructive heuristics (Random Heuristic)
 */
public class ComputeSolution
{
	private final FileRead fileRead;
	private Set<Integer> solutionPoints;
	private Solution solution;
	private static int iterations;
	private static long[] rand;
	private static int randCount;
	
	
	static
	{
		ComputeSolution.iterations = 100000;
		ComputeSolution.randCount = 0;
		ComputeSolution.rand = new long[ComputeSolution.iterations];
		for(int i = 0; i < ComputeSolution.iterations; i++)
		{
			ComputeSolution.rand[i] = i;
		}
	}
	
	public ComputeSolution(FileRead fileRead)
	{
		if(ComputeSolution.randCount >= ComputeSolution.iterations)
			ComputeSolution.randCount = 0;
		this.fileRead = fileRead;
		this.solutionPoints = new HashSet<Integer>(this.fileRead.getChoosen());
		this.solution = new Solution();
	}
	
	public void compute()
	{
		int x;
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		Random rnd = new Random(ComputeSolution.rand[ComputeSolution.randCount++]);
		for(int i = 0; i < this.fileRead.getChoosen(); i++)
		{
			do
			{
				x = rnd.nextInt(matrixDistances.length);
			} while(ComputeSolution.containsPoint(this.solutionPoints, x));
			//Se cogen todos los datos del punto seleccionado
			this.fillSolution(x, matrixDistances[x]);
			this.solutionPoints.add(x);
		}
		
		//System.out.println("Solución: " + this.solution);
		//System.out.println("Mínimo valor de la solución: " + this.solution.minValue());
	}
	
	private static boolean containsPoint(Set<Integer> solutionPoints, int p) 
	{
		for(int i : solutionPoints)
		{
			if(i == p)
			{
				return true;
			}
		}
		return false;
	}

	private void fillSolution(int pos_x, float[] vectorDistances)
	{
		List<Point> points = this.solution.getPoints();
		for(int i : this.solutionPoints)
		{
			points.add(new Point(pos_x, i, vectorDistances[i]));
		}
	}

	public FileRead getFileRead() {
		return fileRead;
	}

	public Set<Integer> getSolutionPoints() {
		return solutionPoints;
	}

	public Solution getSolution() {
		return solution;
	}

	public static int getIterations() {
		return iterations;
	}

	public static void setIterations(int iterations) {
		ComputeSolution.iterations = iterations;
	}

	public static long[] getRand() {
		return rand;
	}

	public static void setRand(long[] rand) {
		ComputeSolution.rand = rand;
	}

	public static int getRandCount() {
		return randCount;
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[fileRead="); 
		sb.append(this.fileRead.getFileName() + ", " + System.lineSeparator() + "solutionPoints=[");
		for(int i : this.solutionPoints)
		{
			sb.append(i + ", ");
		}
		//sb.insert(sb.capacity() - 2, "]");
		sb.append("]," + System.lineSeparator() + "solution=" + this.solution);
		sb.append("," + System.lineSeparator() + "Objetive Function= " + this.solution.minValue() + "]");
		return sb.toString();
	}
	
}
