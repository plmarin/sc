package compute;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import reading.FileRead;
import solution.Solution;

/*
 * This class compute a solution from giving solution
 * through some metaheuristics (stipulated in the Algorithm class)
 */
public class MetaheuristicAlgorithm implements ComputeAlgorithm
{
	private final long TOTAL_TIME;
	private static final int TOTAL_ITERATIONS;
	private final long INITIAL_TIME;
	private final FileRead FILE_READ;
	private Solution solution;
	private Algorithm algorithm;
	private int iterations;
	
	
	static
	{
		TOTAL_ITERATIONS = 1000; // MaxIter
	}

	public MetaheuristicAlgorithm(long time, LocalSearchAlgorithm localSearchAlgorithm, long totalTime) 
	{
		this(time,
			localSearchAlgorithm.getFileRead(),
			localSearchAlgorithm.getSolution(),
			totalTime);
	}
	
	public MetaheuristicAlgorithm(long time, FileRead fileRead, Solution solution, long totalTime) 
	{
		this.INITIAL_TIME = time;
		this.FILE_READ = fileRead;
		this.solution = new Solution(solution);
		this.TOTAL_TIME = totalTime;
		this.iterations = 0;
	}
	
	// Compute Algorithm
	@Override
	public void compute(Algorithm algorithm)
	{
		this.algorithm = algorithm;
		if(this.algorithm.name().equalsIgnoreCase(Algorithm.ITERATED_GREEDY.name()))
		{
			this.iteratedGreedy();
		}
		else if(this.algorithm.name().equalsIgnoreCase(Algorithm.BASIC_VARIABLE_NEIGHBOURHOOD_SEARCH.name()))
		{
			this.basicVariableNeighbourhoodSearch(); 
		}
		else if(this.algorithm.name().equalsIgnoreCase(Algorithm.GRASP.name()))
		{
			this.grasp();
		}
		else
		{
			System.err.println(algorithm.name() + " algorithm hasn't implement yet." +
					" Try with other constructive algorithm");
			System.exit(-1);
		}
	}

	/*
	 * BASIC VARIABLE NEIGHBOURHOOD SEARCH
	 * 
	 * 1. procedimiento BVNS(f, kmax, tmax)
	 * 2. 	repetir
	 * 3. 		k <-- 1;
	 * 4.		repetir
	 * 5.			f' <-- Shake(f, k);  // Randomization
	 * 6.			f'' <-- LocalSearch(f', k);
	 * 7.			NeighbourhoodChange(f, f'', k);
	 * 8.		hasta k = kmax;
	 * 9.		t <-- CPUTime();
	 * 10.	hasta t > tmax;
	 * 11.fin BVNS;
	 */
	private void basicVariableNeighbourhoodSearch() 
	{
		System.err.println(Algorithm.BASIC_VARIABLE_NEIGHBOURHOOD_SEARCH.name() + " algorithm hasn't implement yet." +
				" Try with other metaheuristic algorithm");
		System.exit(-1);
	}

	/*
	 * ITERATED GREEDY
	 * 
	 * 1. procedimiento IG
	 * 2. 	x <-- ConstruirSolucionInicial();
	 * 3. 	x' <-- BusquedaLocal(x);
	 * 4.	mientras (not CriterioParada) hacer
	 * 5.		solParcial <-- DestruirSolucion(x');
	 * 6.		x'' <-- ConstruirSolucion(solParcial);
	 * 7.		x' <-- CriterioAceptacion(x'', x');
	 * 8.	fin mientras;
	 * 9.	devolver x';
	 * 10. fin IG;
	 */
	private void iteratedGreedy() 
	{
		System.err.println(Algorithm.ITERATED_GREEDY.name() + " algorithm hasn't implement yet." +
				" Try with other metaheuristic algorithm");
		System.exit(-1);
	}
	
	/*
	 * GRASP
	 * 
	 * 1. procedimiento GRASP(MaxIter)
	 * 2. 	f' <-- Infinite;
	 * 3.	i <-- 0;
	 * 4.	para i <= MaxIter hacer
	 * 5.		x <-- GreedyRandomizedConstruction();
	 * 6.		x'' <-- LocalSearch(x);
	 * 7.		si (f(x'') < f') entonces
	 * 8.			f' <-- f (x'');
	 * 9.			x' <-- x'';
	 * 10.		fin si;
	 * 11.		i <-- i + 1;
	 * 12.	fin para;
	 * 13.	devolver x';
	 * 14. fin GRASP;
	 */
	private void grasp()
	{
		Solution s;
		LocalSearchAlgorithm lsa = new LocalSearchAlgorithm(this.FILE_READ);
		do
		{
			s = GreedyRandomizedConstruction();
			lsa.setSolution(s);
			lsa.compute(Algorithm.FIRST_RANDOM_IMPROVEMENT);
			s = lsa.getSolution();
			if(this.solution.compareTo(s) == 1)
			{
				this.solution = new Solution(s);
			}
		}while(! this.stopCondition());
	}

	private Solution GreedyRandomizedConstruction()
	{
		Solution s = new Solution(this.FILE_READ);
		List<Integer> cl = new ArrayList<Integer>(s.getNodeArray().length);
		for(int i = 0; i < s.getNodeArray().length; i++)
		{
			cl.add(s.getNodeArray()[i].getId());
		}
		Random rnd = new Random(this.iterations);
		List<Integer> rcl = this.buildRCL(s);
		for(int i = 0; i < s.getHubsIndex().length; i++)
		{
			s.addHub(rcl.remove(rnd.nextInt(rcl.size())));
		}		
		for(int i = 0; i < s.getHubsIndex().length; i++)
		{
			cl.remove((Integer) (s.getHubsIndex()[i] + 1));
		}
		
		int hubIndex;
		int nodeIndex;
		int busyNodes = s.getHubsAdded();
		int maxIterations = s.getHubsAdded() * 2;
		int iterations = 0;
		do
		{
			hubIndex = rnd.nextInt(s.getHubsAdded());
			nodeIndex = s.nearestNodeIndex(hubIndex);
			if(s.addNode(hubIndex, nodeIndex))
			{
				busyNodes++;
				iterations = 0;
			}
			else
			{
				if(iterations == maxIterations)
				{
					return this.discardSolution(s);
				}
				iterations++;
			}
		}while(busyNodes < (cl.size() + s.getHubsAdded()));
		
		return s;
	}
	
	private List<Integer> buildRCL(Solution s) 
	{
		final int SIZE_RCL = s.getHubsIndex().length * 3;
		List<Integer> rcl = new ArrayList<Integer>();
		for(int i = 0; i < SIZE_RCL; i++)
		{
			rcl.add(i, s.getIndexMaxDistanceAllDistances(rcl));
		}
		return rcl;
	}
	
	private boolean stopCondition() 
	{
		boolean stopCondition;

		// Time
		stopCondition = System.currentTimeMillis() - this.INITIAL_TIME > this.TOTAL_TIME;
		// Iterations
		stopCondition = stopCondition || this.iterations++ > MetaheuristicAlgorithm.TOTAL_ITERATIONS;

		return stopCondition;
	}
	
	private Solution discardSolution(Solution solution) 
	{
		int sizeHubIndexList = solution.getHubsAdded();
		float[] distances = new float[sizeHubIndexList];
		for(int i = 0; i < distances.length; i++)
		{
			distances[i] = Float.MAX_VALUE / (sizeHubIndexList + 1);
		}
		solution.setDistances(distances);
		
		return solution;
	}

	public FileRead getFileRead() 
	{
		return this.FILE_READ;
	}
	
	@Override
	public Solution getSolution() 
	{
		return this.solution;
	}
	
	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[absolutePathFileRead= \"" + this.FILE_READ.getAbosultePathFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("fileRead= \"" + this.FILE_READ.getFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("solution= " + this.solution);
		sb.append("," + System.lineSeparator());	
		sb.append("Objetive Function= " + this.solution.getDistance() + "]");
		return sb.toString();
	}

}
