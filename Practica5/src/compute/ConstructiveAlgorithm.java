package compute;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import reading.FileRead;
import solution.Solution;

/*
 * This class compute a solution through some constructive heuristics (stipulated in the Algorithm class)
 */
public class ConstructiveAlgorithm implements ComputeAlgorithm
{	
	private final FileRead FILE_READ;
	private Solution solution;
	private static final int ITERATIONS;
	private static final long[] RAND;
	private static int randCount;
	
	
	static
	{
		ITERATIONS = 1;
		RAND = new long[ConstructiveAlgorithm.ITERATIONS];
		for(int i = 0; i < ConstructiveAlgorithm.ITERATIONS; i++)
		{
			ConstructiveAlgorithm.RAND[i] = i;
		}
		ConstructiveAlgorithm.randCount = 0;
	}
	
	public ConstructiveAlgorithm(FileRead fileRead)
	{
		this(fileRead, new Solution(fileRead));
	}
	
	public ConstructiveAlgorithm(FileRead fileRead, Solution solution)
	{
		if(ConstructiveAlgorithm.randCount >= ConstructiveAlgorithm.ITERATIONS)
			ConstructiveAlgorithm.randCount = 0;
		this.FILE_READ = fileRead;
		this.solution = solution;
	}
	
	// Compute Algorithm
	@Override
	public void compute(Algorithm algorithm)
	{
		if(algorithm.name().equals(Algorithm.VORACIOUS.name()))
		{
			this.voracious();
		}
		else if(algorithm.name().equals(Algorithm.RANDOM.name()))
		{
			this.random();
		}
		else
		{
			System.err.println(algorithm.name() + " algorithm hasn't implement yet." +
					" Try with other constructive algorithm");
			System.exit(-1);
		}
	}
	
	/*
	 * RANDOM HEURISTIC
	 */
	private void random()
	{
		Solution oldSolution = new Solution(this.solution);
		
		Random rnd = new Random(ConstructiveAlgorithm.RAND[ConstructiveAlgorithm.randCount++]);
		List<Integer> indexList = this.solution.getIndexList();
		List<Integer> aux = new ArrayList<Integer>(indexList.size());
		do
		{
			int element = rnd.nextInt(indexList.size());
			if(! aux.contains(element))
			{
				aux.add(element);
			}
		}while(aux.size() < indexList.size());
		
		this.solution.setIndexList(aux);
		
		if(this.solution.maxCut() >= oldSolution.maxCut())
		{
			this.solution = oldSolution;
		}
	}
	
	/*
	 * VORACIOUS HEURISTIC
	 */
	private void voracious()
	{		
		// Method 1: Take the element that has the least adjacent each time
		Solution oldSolution = new Solution(this.solution);
		
		List<Integer> indexList = new ArrayList<Integer>(this.solution.getIndexList().size());
		List<List<Integer>> copyOfAdjacentList = this.FILE_READ.getCopyOfAdjacentList();
		for(int i = 0; i < this.solution.getIndexList().size(); i++)
		{
			indexList.add(this.indexMinAdjacent(copyOfAdjacentList));
		}
		this.solution.setIndexList(indexList);
		
		if(this.solution.maxCut() > oldSolution.maxCut())
		{
			this.solution = oldSolution;
		}
		
		// Method 2: Take maxCuts at the beginning
		/*Solution oldSolution = new Solution(this.solution);
		
		List<Integer> indexList = new ArrayList<Integer>(this.solution.getCuts().length);
		for(int i = 0; i < this.solution.getCuts().length; i++)
		{
			indexList.add(i, this.indexMaxCuts(this.solution.getCuts()));
		}
		indexList.add(this.solution.getCuts().length, this.addLastElement(indexList, this.solution.getCuts().length));
		this.solution.setIndexList(indexList);
		this.solution.computeCuts();
		
		if(this.solution.maxCut() > oldSolution.maxCut())
		{
			this.solution = oldSolution;
		}*/
	}
	
	@SuppressWarnings("unused")
	private int addLastElement(List<Integer> list, int length) 
	{
		int element = 0;
		boolean success = false;
		do
		{
			if(! list.contains(element))
			{
				success = true;
			}
			else
			{
				element++;
			}
		}while(! success);
		
		return element;
	}

	@SuppressWarnings("unused")
	private int indexMaxCuts(int[] cuts)
	{
		int maxCut = 0;
		int index = -1;
		
		for(int i = 0; i < cuts.length; i++)
		{
			if(maxCut < cuts[i])
			{
				maxCut = cuts[i];
				index = i;
			}
		}
		cuts[index] = 0;
		return index;
	}

	private int indexMinAdjacent(List<List<Integer>> adjacentList)
	{
		int minAdjacent = Integer.MAX_VALUE;
		int index = -1;
		
		for(int i = 0; i < adjacentList.size(); i++)
		{
			if(adjacentList.get(i).size() > 0 && minAdjacent > adjacentList.get(i).size())
			{
				minAdjacent = adjacentList.get(i).size();
				index = i;
			}
		}
		adjacentList.get(index).clear();		
		return index;
	}
	
	public final FileRead getFileRead() 
	{
		return this.FILE_READ;
	}

	@Override
	public Solution getSolution() 
	{
		return this.solution;
	}

	public static int getIterations() 
	{
		return ConstructiveAlgorithm.ITERATIONS;
	}

	public static long[] getRand() 
	{
		return ConstructiveAlgorithm.RAND;
	}

	public static int getRandCount() 
	{
		return ConstructiveAlgorithm.randCount;
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[absolutePathFileRead= \"" + this.FILE_READ.getAbosultePathFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("fileRead= \"" + this.FILE_READ.getFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("solution= " + this.solution);
		sb.append("," + System.lineSeparator());	
		sb.append("Objetive Function= " + this.solution.maxCut() + "]");
		return sb.toString();
	}
	
}
