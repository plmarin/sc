package bean;

import java.util.List;

import utils.Utils;

public class Point implements Comparable<Point>
{
	private int x;
	private int y;
	private float value;
	
	
	public Point()
	{
		
	}
	
	public Point(int x, int y)
	{
		this(x, y, 0f);
	}
	
	public Point(int x, int y, float value) 
	{
		super();
		this.x = x;
		this.y = y;
		this.value = value;
	}
	
	public Point(Point p)
	{
		this.x = p.x;
		this.y = p.y;
		this.value = p.value;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public float getValue()
	{
		return this.value;
	}

	public void setValue(float value)
	{
		this.value = value;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof Point)
		{
			Point p = (Point) obj;
			if((this.x == p.getX() && this.y == p.getY())
					|| (this.x == p.getY() && this.y == p.getX()) 
					&& this.value == p.getValue())
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() 
	{
		return "[x=" + this.x +
				", y=" + this.y +
				", value=" + Utils.roundToNDecimals(this.value, 2) + "]";
	}

	@Override
	public int compareTo(Point p)
	{
		return this.value == p.getValue() ? 0 : (this.value > p.getValue() ? 1 : -1);
	}
	
	public boolean contains(Point[] points)
	{
		return Point.contains(points, this);
	}
	
	public static boolean contains(Point[] points, Point point)
	{
		for(Point p : points)
		{
			if(p != null)
				if(p.equals(point))
					return true;
		}
		return false;
	}
	
	public static Point minValue(List<Point> points)
	{
		Point point = points.get(0);
		for(int i = 1; i < points.size(); i++)
		{
			point = point.getValue() <= points.get(i).getValue() ? point : points.get(i);
		}
		return point;
	}
	
	public static Point maxValue(List<Point> points)
	{
		Point point = points.get(0);
		for(int i = 1; i < points.size(); i++)
		{
			point = point.getValue() >= points.get(i).getValue() ? point : points.get(i);
		}
		return point;
	}
	
}
