package writing;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileWrite
{
	private static final String FILE_NAME;
	private static final Path PATH;
	
	
	static
	{
		String separator = System.getProperty("file.separator");
		FILE_NAME = "Practica4_PabloLuisMarinCano.txt";
		PATH = Paths.get(".." + separator + FileWrite.FILE_NAME);
	}
	
	public static void writeFile(String metaheuristic)
	{
		FileWriter file = null;
        PrintWriter pw = null;
        try
        {
        	file = new FileWriter(FileWrite.PATH.toAbsolutePath().toString(), true);
            pw = new PrintWriter(file);
            
            pw.println("Pablo Luis Marin Cano - " + metaheuristic + System.lineSeparator());
        } 
        catch(Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
        	FileWrite.close(file, pw);
        }
	}
	
	public static void writeSolution(String bestSolution, double seconds) 
	{   
        FileWriter file = null;
        PrintWriter pw = null;
        try
        {
        	file = new FileWriter(FileWrite.PATH.toAbsolutePath().toString(), true);
            pw = new PrintWriter(file);
 
            pw.println(bestSolution);
            pw.println("The task has taken " + seconds + " seconds" + System.lineSeparator() + System.lineSeparator());
            //System.out.println("Fichero escrito en: " + FileWrite.PATH.toAbsolutePath().toString());
        } 
        catch(Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
           FileWrite.close(file, pw);
        }
	}
	
	private static void close(FileWriter file, PrintWriter pw)
	{
		try 
        {
           if(file != null)
           {
              pw.close();
           }
        } 
        catch(Exception e) 
        {
           e.printStackTrace();
        }
	}
	
}
