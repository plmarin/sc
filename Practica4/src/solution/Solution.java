package solution;

import java.util.HashSet;
import java.util.Set;

import utils.Utils;

public class Solution implements Comparable<Solution>
{
	private static float[][] matrixDistances;
	private Set<Integer> points;
	
	
	static
	{
		Solution.matrixDistances = null;
	}
	
	public Solution()
	{
		this(new HashSet<Integer>());
	}
	
	public Solution(int initialCapacity)
	{
		this(new HashSet<Integer>(initialCapacity));
	}
	
	public Solution(Set<Integer> points)
	{
		super();
		this.points = points;
	}
	
	public Solution(Solution solution)
	{
		super();
		this.points = new HashSet<Integer>(solution.getPoints());
	}
	
	public void addPoint(Integer point)
	{
		this.points.add(point);
	}
	
	public void removePoint(Integer point)
	{
		this.points.remove(point);
	}
	
	public int size()
	{
		return this.points.size();
	}
	
	@Override
	public int compareTo(Solution s)
	{
		float minValue = this.minValue();
		float sMinValue = s.minValue();
		return minValue == sMinValue ? 0 : (minValue < sMinValue ? -1 : 1);
	}
	
	// Objective Function
	public float minValue()
	{
		Integer[] points = (Integer[]) this.points.toArray(new Integer[this.points.size()]);
		float minValue = Float.MAX_VALUE;
		for(int i = 0; i < points.length; i++)
		{
			for(int j = i + 1; j < points.length; j++)
			{
				if(minValue > Solution.matrixDistances[points[i]][points[j]])
				{
					minValue = Solution.matrixDistances[points[i]][points[j]];
				}
			}
		}
		return minValue;
	}
	
	public float maxValue()
	{
		Integer[] points = (Integer[]) this.points.toArray();
		float maxValue = 0.0f;
		for(int i = 0; i < points.length; i++)
		{
			for(int j = i+1; j < points.length; j++)
			{
				if(maxValue < Solution.matrixDistances[points[i]][points[j]])
				{
					maxValue = Solution.matrixDistances[points[i]][points[j]];
				}
			}
		}
		return maxValue;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(obj instanceof Solution)
		{
			Solution s = (Solution) obj;
			if(this.points.size() == s.getPoints().size())
			{
				if(this.points.containsAll(s.getPoints()))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean contains(Integer point)
	{
		return this.points.contains(point) ? true : false;
	}
	
	public Set<Integer> getPoints() 
	{
		return this.points;
	}

	public void setPoints(Set<Integer> points)
	{
		this.points = points;
	}
	
	public void addPoints(Set<Integer> points)
	{
		this.points.addAll(points);
	}
	
	public static float[][] getMatrixDistances() 
	{
		return Solution.matrixDistances;
	}

	public static void setMatrixDistances(float[][] matrixDistances)
	{
		Solution.matrixDistances = matrixDistances;
	}
	
	public String results()
	{
		Integer[] points = (Integer[]) this.points.toArray(new Integer[this.points.size()]);
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < points.length; i++)
		{
			for(int j = i + 1; j < points.length; j++)
			{
				sb.append(Utils.roundToNDecimals(Solution.matrixDistances[points[i]][points[j]], 2) + ", ");
			}
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}

	@Override
	public String toString()
	{
		return this.points.toString();
	}

}
