package bean;

public class Node
{
	private int id;
	private Point point;
	private int demandValue;
	private boolean hub;
	private int capacity;
	private boolean free;
	
	
	public Node(int id, int x, int y, int demandValue, int capacity)
	{
		this(id, x, y, demandValue, false, capacity);
	}
	
	public Node(int id, Point point, int demandValue, int capacity)
	{
		this(id, point, demandValue, false, capacity);
	}
	
	public Node(int id, int x, int y, int demandValue, boolean hub, int capacity)
	{
		this(id, new Point(x, y), demandValue, hub, capacity);
	}
	
	public Node(int id, Point point, int demandValue, boolean hub, int capacity)
	{
		this.id = id;
		this.point = point;
		this.demandValue = demandValue;
		this.hub = hub;
		this.capacity = capacity;
		this.free = true;
	}

	public Node(Node node)
	{
		this.id = node.getId();
		this.point = new Point(node.getPoint());
		this.demandValue = node.getDemandValue();
		this.hub = node.isHub();
		this.capacity = node.getCapacity();
		this.free = node.isFree();
	}

	public int getId()
	{
		return this.id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public Point getPoint()
	{
		return this.point;
	}

	public void setPoint(Point point) 
	{
		this.point = point;
	}

	public void setPoint(int x, int y) 
	{
		this.point.setX(x);
		this.point.setY(y);
	}

	public int getDemandValue() 
	{
		return this.demandValue;
	}
	
	public int getNodeDemandValue()
	{
		return this.isHub() ? 0 : this.demandValue;
	}

	public void setDemandValue(int demandValue) 
	{
		this.demandValue = demandValue;
	}

	public boolean isHub() 
	{
		return this.hub;
	}

	public void setHub(boolean hub)
	{
		this.hub = hub;
		if(hub)
		{
			this.free = false;
		}
	}

	public int getCapacity() 
	{
		return this.capacity;
	}

	public void setCapacity(int capacity) 
	{
		this.capacity = capacity;
	}

	public boolean isFree() 
	{
		return this.free;
	}

	public void setFree(boolean free) 
	{
		this.free = free;
	}
	
	public void printProperties()
	{
		System.out.println("Node [id = " + this.id + ", " + this.point + ", demandValue = "
				+ this.demandValue + ", hub = " + this.hub + ", capacity = " 
				+ this.capacity + ", free = " + this.free + "]");
	}

	@Override
	public String toString() 
	{
		return this.id + "";
	}
	
}
