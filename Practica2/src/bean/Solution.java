package bean;

import java.util.ArrayList;
import java.util.List;

public class Solution implements Comparable<Solution>
{
	private List<Point> points;
	
	
	public Solution(int x, int y, float value) 
	{
		this(new Point(x, y, value));
	}
	
	public Solution(Point point)
	{
		super();
		this.points = new ArrayList<Point>();
		this.points.add(point);
	}
	
	public Solution(List<Point> points)
	{
		super();
		this.points = points;
	}
	
	public Solution()
	{
		this(new ArrayList<Point>());
	}
	
	public Point getPoint(int i)
	{
		return this.points.get(i);
	}
	
	public void addPoint(Point point)
	{
		this.points.add(point);
	}

	public List<Point> getPoints() 
	{
		return this.points;
	}

	public void setPoints(List<Point> points)
	{
		this.points = points;
	}
	
	public void addPoints(List<Point> points)
	{
		this.points.addAll(points);
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder("[");
		for(Point point : this.points)
		{
			sb.append(point + "," + System.lineSeparator());
		}
		sb.append("]");
		//sb.replace(sb.capacity() - 2, sb.capacity() - 1, "]");
		return sb.toString();
	}

	@Override
	public int compareTo(Solution s)
	{
		return this.minValue().compareTo(s.minValue());
	}
	
	public Point minValue()
	{
		Point point = this.getPoint(0);
		for(Point p : this.points)
		{
			point = point.getValue() <= p.getValue() ? point : p;
		}
		return point;
	}
	
	public Point maxValue()
	{
		Point point = this.getPoint(0);
		for(Point p : this.points)
		{
			point = point.getValue() >= p.getValue() ? point : p;
		}
		return point;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(obj instanceof Solution)
		{
			Solution s = (Solution) obj;
			if(this.points.size() == s.getPoints().size())
			{
				for(int i = 0; i < this.points.size(); i++)
				{
					if(! this.points.get(i).equals(s.getPoints().get(i)))
					{
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}
	
	public boolean contains(Point point)
	{
		for(Point p : this.points)
		{
			if(p.equals(point))
				return true;
		}
		return false;
	}

}
