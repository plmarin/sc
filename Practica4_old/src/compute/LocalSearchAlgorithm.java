package compute;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import reading.FileRead;
import bean.Point;
import bean.Solution;

/*
 * This class compute a solution from giving solution
 * through some local search heuristics (stipulated in the Algorithm class)
 */
public class LocalSearchAlgorithm 
{
	private final FileRead fileRead;
	private Set<Integer> solutionPoints;
	private Solution solution;

	
	public LocalSearchAlgorithm(ConstructiveAlgorithm constructiveAlgorithm) 
	{
		this(constructiveAlgorithm.getFileRead(),
			constructiveAlgorithm.getSolutionPoints(), 
			constructiveAlgorithm.getSolution());
	}
	
	public LocalSearchAlgorithm(FileRead fileRead, Set<Integer> solutionPoints, Solution solution) 
	{
		this.fileRead = fileRead;
		this.solutionPoints = new HashSet<Integer>(solutionPoints);
		this.solution = new Solution(solution);
	}
	
	//Compute Algorithm
	public void compute(Algorithm algorithm)
	{
		if(algorithm.name().equals(Algorithm.BEST_IMPROVEMENT.name()))
		{
			this.bestImprovement();
		}
		else if(algorithm.name().equalsIgnoreCase(Algorithm.FIRST_LEXICOGRAPHICAL_IMPROVEMENT.name()))
		{
			this.firstLexicographicalImprovement();
		}
		else
		{
			this.firstRandomImprovement();
		}
	}
	
	/*
	 * FIRST RANDOM IMPROVEMENT
	 */
	private void firstRandomImprovement() 
	{
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		Object[] oldSolutionPoints;
		Set<Integer> selectedPoints = new HashSet<Integer>();
		Random rnd = new Random(this.fileRead.getChoosen());
		Point bestSolution = this.solution.minValue();
		Point minValue;
		int solutionPointsSize = this.solutionPoints.size();
		int fileReadSize = this.fileRead.getSize();
		int solutionPoint;
		boolean improve;
		
		//Iterar hasta que no haya mejora
		do
		{
			improve = false;
			oldSolutionPoints = this.solutionPoints.toArray();
			for(int i = 0; i < solutionPointsSize; i++)
			{
				do
				{
					solutionPoint = (int) oldSolutionPoints[rnd.nextInt(oldSolutionPoints.length)];
				}while(selectedPoints.contains(solutionPoint));
				selectedPoints.add(solutionPoint);
				
				this.solution.removePoint(solutionPoint);
				this.solutionPoints.remove(solutionPoint);			
				
				//Busqueda de una solucion mejor a la anterior
				//Se elimina un punto y se prueba el siguiente
				//Se escoge la mejor solucion y se pasa a la siguiente iteracion
				for(int j = 0; j < fileReadSize; j++)
				{
					//Si se va a examinar el punto elegido o un punto que ya esta
					//en la solucion se salta a la siguiente iteracion
					if(j == solutionPoint || this.solutionPoints.contains(j))
					{
						continue;
					}
					
					this.fillSolution(j, matrixDistances[j]);
					this.solutionPoints.add(j);
					minValue = this.solution.minValue();
					
					//Si se obtiene una mejor solucion se acepta
					//y no se comprueban mas posibilidades con la solucion anterior
					if(minValue.compareTo(bestSolution) == 1)
					{
						improve = true;
						bestSolution = minValue;
						break;
					}
					this.solution.removePoint(j);
					this.solutionPoints.remove(j);
				}
				if(improve)
				{
					selectedPoints.clear();
					break;
				}
				this.fillSolution(solutionPoint, matrixDistances[solutionPoint]);
				this.solutionPoints.add(solutionPoint);
			}
		}while(improve);
	}

	/*
	 * FIRST LEXICOGRAPHICAL IMPROVEMENT
	 */
	private void firstLexicographicalImprovement() 
	{
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		Set<Integer> oldSolutionPoints = new HashSet<Integer>(this.solutionPoints);
		Solution bestSolution = new Solution(this.solution);
		int fileReadSize = this.fileRead.getSize();
		int compare;
		boolean improve;
		
		//Iterar hasta que no haya mejora
		do
		{
			improve = false;
			for(int solutionPoint : oldSolutionPoints)
			{
				this.solution.removePoint(solutionPoint);
				this.solutionPoints.remove(solutionPoint);			
				
				//Busqueda de una solucion mejor a la anterior
				//Se elimina un punto y se prueba el siguiente
				//Se escoge la mejor solucion y se pasa a la siguiente iteracion
				for(int i = 0; i < fileReadSize; i++)
				{
					if(i == solutionPoint || this.solutionPoints.contains(i))
					{
						continue;
					}
					
					this.fillSolution(i, matrixDistances[i]);
					this.solutionPoints.add(i);
					compare = this.solution.compareTo(bestSolution);
					if(compare == 1)
					{
						improve = true;
						bestSolution = new Solution(this.solution);
						oldSolutionPoints.remove(solutionPoint);
						oldSolutionPoints.add(i);
						break;
					}
					this.solution.removePoint(i);
					this.solutionPoints.remove(i);
				}
				if(improve)
				{
					break;
				}
				this.fillSolution(solutionPoint, matrixDistances[solutionPoint]);
				this.solutionPoints.add(solutionPoint);
			}
		}while(improve);
	}

	/*
	 * BEST IMPROVEMENT
	 */
	@SuppressWarnings("unchecked")
	private void bestImprovement() 
	{
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		Set<Integer> oldSolutionPoints;
		Random rnd = new Random(this.fileRead.getChoosen());
		
		//Iterar hasta que no haya mejora
		do
		{
			oldSolutionPoints = new HashSet<Integer>(this.solutionPoints);
			for(int solutionPoint : oldSolutionPoints)
			{
				Map<Solution, Set<Integer>> bestSolutions = new HashMap<Solution, Set<Integer>>();
				Solution bestSolution = new Solution(this.solution);
				bestSolutions.put(bestSolution, new HashSet<Integer>(this.solutionPoints));
				this.solution.removeXPoints(solutionPoint);
				this.solution.removeYPoints(solutionPoint);
				this.solutionPoints.remove(solutionPoint);
				boolean improve = false;
				int compare;
				
				//Busqueda de una solucion mejor a la anterior
				//Se elimina un punto y se prueba el siguiente
				//Se guardan todas las posibles soluciones en el Map: bestSolutions
				for(int i = 0; i < this.fileRead.getSize(); i++)
				{
					if(i == solutionPoint || this.solutionPoints.contains(i))
					{
						continue;
					}
					
					this.fillSolution(i, matrixDistances[i]);
					this.solutionPoints.add(i);
					compare = this.solution.compareTo(bestSolution);
					if(compare == 1)
					{
						improve = true;
						bestSolutions.clear();
						bestSolution = new Solution(this.solution);
						bestSolutions.put(bestSolution, new HashSet<Integer>(this.solutionPoints));
					}
					else if(compare == 0)
					{
						bestSolutions.put(new Solution(this.solution), new HashSet<Integer>(this.solutionPoints));
					}
					this.solution.removeXPoints(i);
					this.solution.removeYPoints(i);
					this.solutionPoints.remove(i);
				}
				this.fillSolution(solutionPoint, matrixDistances[solutionPoint]);
				this.solutionPoints.add(solutionPoint);
				
				//Si hay varias "best solutions" se escoge una al azar
				//siempre que se haya mejorado la solucion anterior
				if(bestSolutions.size() > 1 && improve)
				{
					int aux = rnd.nextInt(bestSolutions.size());
					this.solution = (Solution) bestSolutions.keySet().toArray()[aux];
					this.solutionPoints = (Set<Integer>) bestSolutions.values().toArray()[aux];
				}
			}
		}while(! oldSolutionPoints.containsAll(this.solutionPoints));
	}

	private void fillSolution(int pos_x, float[] vectorDistances)
	{
		List<Point> points = this.solution.getPoints();
		for(int i : this.solutionPoints)
		{
			points.add(new Point(pos_x, i, vectorDistances[i]));
		}
	}

	public FileRead getFileRead() 
	{
		return this.fileRead;
	}

	public Set<Integer> getSolutionPoints() 
	{
		return this.solutionPoints;
	}

	public Solution getSolution() 
	{
		return this.solution;
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[fileRead=");
		sb.append(this.fileRead.getFileName() + ", " + System.lineSeparator());
		/*
		sb.append(this.fileRead.getFileName() + ", " + System.lineSeparator() + "solutionPoints=[");
		for(int i : this.solutionPoints)
		{
			sb.append(i + ", ");
		}
		//sb.insert(sb.capacity() - 2, "]");
		sb.append("]," + System.lineSeparator() + "solution=" + this.solution);
		sb.append("," + System.lineSeparator() + "Objetive Function= " + this.solution.minValue() + "]");
		*/
		sb.append("solution= " + this.solutionPoints);
		sb.append("," + System.lineSeparator());
		sb.append("Objetive Function= " + this.solution.minValue() + "]");
		return sb.toString();
	}

}
