package utils;

import bean.Point;

public class Utils 
{
	public static float roundToNDecimals(float number, int decimals)
	{
		return (float) (Math.round(number * Math.pow(10, decimals)) / Math.pow(10, decimals));
	}
	
	public static float euclideanDistance(Point p1, Point p2)
	{
		int sum = (int) (Math.pow(p2.getX() - p1.getX(), 2) + Math.pow(p2.getY() - p1.getY(), 2));
		float euclideanDistance = (float) Math.pow(sum, 0.5d);
		return euclideanDistance;
	}
	
}
