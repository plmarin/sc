package main;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import reading.FileRead;
import utils.Utils;
import writing.FileWrite;

import compute.Algorithm;
import compute.ComputeAlgorithm;
import compute.ConstructiveAlgorithm;
import compute.LocalSearchAlgorithm;
import compute.MetaheuristicAlgorithm;

public class Main
{
	private static Algorithm constructiveAlgorithm;
	private static Algorithm localSearchAlgorithm;
	private static Algorithm metaheuristicAlgorithm;
	private static boolean writeFile;
	private static final long TIME_PER_INSTANCE;
	private static long timePerInstance;
	
	
	static
	{
		//TOTAL_TIME = 20L * 60L * 1000L; // Max: 20 minutes (in milliseconds)
		TIME_PER_INSTANCE = 60L * 1000L; // 1 minute (in milliseconds)
		Main.timePerInstance = Main.TIME_PER_INSTANCE;
		Main.constructiveAlgorithm = Algorithm.VORACIOUS_MULTIBOOT;
		Main.localSearchAlgorithm = Algorithm.FIRST_LEXICOGRAPHICAL_IMPROVEMENT;
		Main.metaheuristicAlgorithm = Algorithm.GRASP;
		Main.writeFile = false;
	}
	
	public static void main(String[] args) 
	{
		Path path = Main.getPath(args);
		
		System.out.println("Pablo Luis Marin Cano - " + Main.metaheuristicAlgorithm + System.lineSeparator());
		
		File file = path.toFile();
		if(! file.exists())
		{
			// Si no existe la ruta especificada se le asigna la instancia de las practicas por defecto
			String separator = System.getProperty("file.separator");
			String defaultPath = "Instancias_CPH" + separator + "instancias" + separator;
			file = new File(defaultPath);
			
			// Si sigue sin existir se sale del programa con la notificacion del error
			if(! file.exists())
			{
				System.err.println("No se pudo acceder a la ruta: " + System.lineSeparator() +
						" \"" + path.toAbsolutePath().normalize().toString() + "\"." + System.lineSeparator() +
						"Tampoco se pudo acceder a la ruta por defecto: " + System.lineSeparator() +
						" \"" + Paths.get(defaultPath).toAbsolutePath().toString() + "\".");
				System.exit(-1);
			}
		}
		
		if(Main.writeFile)
		{
			FileWrite.writeFile(Main.metaheuristicAlgorithm.toString());
		}
		
		// Inicio computo
		final long timeStart;
		final long timeEnd;
		final double totalTime;
		final int minutes;
		final Float seconds;
		timeStart = System.currentTimeMillis();
		
		// A directory
		Main.init(file);
		// A specific file
		//Main.init(Paths.get(file.toString() + "/phub_50_5_1.txt").toFile());
		
		// Fin computo
		timeEnd = System.currentTimeMillis();
		totalTime = (timeEnd - timeStart) / 1000.0d;
		minutes = (int) (totalTime / 60);
		seconds = (float) (totalTime % 60.0f);
		
		System.out.println(System.lineSeparator() +
				"The task has taken " + totalTime + " seconds: " + minutes + " minutes " +
				seconds.intValue() + " seconds " + 
				(int) ((seconds - seconds.intValue()) * 1000) + " milliseconds.");
	}

	// Begin of the compute of solution
	private static void init(File file) 
	{
		if(file.isDirectory())
		{
			List<File> files = Arrays.asList(file.listFiles());
			Collections.sort(files);
			for(File f : files)
			{
				Main.compute(f);
			}
		}
		else
		{
			Main.compute(file);
		}		
	}

	private static void compute(File file)
	{
		final long TIME = System.currentTimeMillis();
		long instanceTime;
				
		// File read 
		FileRead fr = new FileRead(file.getAbsolutePath()); 

		// Constructive algorithm
		ConstructiveAlgorithm[] cas = new ConstructiveAlgorithm[ConstructiveAlgorithm.getIterations()];
		for(int i = 0; i < ConstructiveAlgorithm.getIterations(); i++)
		{
			cas[i] = new ConstructiveAlgorithm(fr);
			cas[i].compute(Main.constructiveAlgorithm);
		}		
		//ConstructiveAlgorithm caBestSolution = (ConstructiveAlgorithm) Main.getBestSolution(cas);
		
		// Local search algorithm
		LocalSearchAlgorithm[] lsas = new LocalSearchAlgorithm[cas.length];
		for(int i = 0; i < cas.length; i++)
		{
			lsas[i] = new LocalSearchAlgorithm(cas[i]);
			lsas[i].compute(Main.localSearchAlgorithm);
		}
		LocalSearchAlgorithm lsaBestSolution = (LocalSearchAlgorithm) Main.getBestSolution(lsas);
		
		// Metaheuristic algorithm
		MetaheuristicAlgorithm ma = new MetaheuristicAlgorithm(TIME, lsaBestSolution, Main.timePerInstance);
		ma.compute(Main.metaheuristicAlgorithm);
		
		String out = ma.getFileRead().getFileName() + "\t" +
				Utils.roundToNDecimals(ma.getSolution().getDistance(), 2) + "\t" +  
				ma.getSolution() + System.lineSeparator();
		System.out.println(out);
		
		long extraTime;
		instanceTime = System.currentTimeMillis() - TIME;
		if(instanceTime < Main.TIME_PER_INSTANCE)
		{
			extraTime = Main.timePerInstance - instanceTime;
			Main.timePerInstance += extraTime;
		}
		else
		{
			if(instanceTime < Main.timePerInstance)
			{
				extraTime = Main.timePerInstance - instanceTime;
				Main.timePerInstance = Main.TIME_PER_INSTANCE + extraTime;
			}
			else
			{
				Main.timePerInstance = Main.TIME_PER_INSTANCE;
			}
		}
		
		if(Main.writeFile)
		{
			FileWrite.writeSolution(out, instanceTime / 1000.0d);
		}	
	}

	private static ComputeAlgorithm getBestSolution(ComputeAlgorithm[] computeAlgorithm) 
	{
		ComputeAlgorithm ca = computeAlgorithm[0];
		for(int i = 0; i < computeAlgorithm.length; i++)
		{
			ca = ca.getSolution().getDistance() >= computeAlgorithm[i].getSolution().getDistance() ? computeAlgorithm[i] : ca;
		}
		return ca;
	}
	
	// This function obtains the path where the instances are stored
	private static Path getPath(String[] args) 
	{
		Path path;	
		// Default values
		if(args.length == 0)
		{
			String separator = System.getProperty("file.separator");
	        path = Paths.get("Instancias_CPH" + separator);
		}
		// Command line values
		else
		{
			path = Paths.get(args[0]);
			if(args.length > 1)
			{
				Main.constructiveAlgorithm = Main.setConstructiveAlgorithm(args[1]);
				if(args.length > 2)
				{
					Main.localSearchAlgorithm = Main.setLocalSearchAlgorithm(args[2]);
					if(args.length > 3)
					{
						Main.metaheuristicAlgorithm = Main.setMetaheuristicAlgorithm(args[3]);
						if(args.length > 4)
						{
							Main.writeFile = args[4].equalsIgnoreCase("true") ? true : false;
						}
					}
				}
			}
		}
		return path;
	}

	// Options to command line input values
	private static Algorithm setConstructiveAlgorithm(String algorithm)
	{
		Algorithm a;
		if(algorithm.equalsIgnoreCase(Algorithm.VORACIOUS.name()))
		{
			a = Algorithm.VORACIOUS;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.VORACIOUS_MULTIBOOT.name()))
		{
			a = Algorithm.VORACIOUS_MULTIBOOT;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.DESTRUCTIVE_VORACIOUS.name()))
		{
			a = Algorithm.DESTRUCTIVE_VORACIOUS;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.DESCOMPOSITION.name()))
		{
			a = Algorithm.DESCOMPOSITION;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.REDUCTION.name()))
		{
			a = Algorithm.REDUCTION;
		}
		else
		{
			a = Algorithm.RANDOM;
		}
		return a;
	}
	
	// Options to command line input values
	private static Algorithm setLocalSearchAlgorithm(String algorithm)
	{
		Algorithm a;
		if(algorithm.equalsIgnoreCase(Algorithm.BEST_IMPROVEMENT.name()))
		{
			a = Algorithm.BEST_IMPROVEMENT;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.FIRST_LEXICOGRAPHICAL_IMPROVEMENT.name()))
		{
			a = Algorithm.FIRST_LEXICOGRAPHICAL_IMPROVEMENT;
		}
		else
		{
			a = Algorithm.FIRST_RANDOM_IMPROVEMENT;
		}
		return a;
	}
	
	// Options to command line input values
	private static Algorithm setMetaheuristicAlgorithm(String algorithm) 
	{
		Algorithm a;
		if(algorithm.equalsIgnoreCase(Algorithm.ITERATED_GREEDY.name()))
		{
			a = Algorithm.ITERATED_GREEDY;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.BASIC_VARIABLE_NEIGHBOURHOOD_SEARCH.name()))
		{
			a = Algorithm.BASIC_VARIABLE_NEIGHBOURHOOD_SEARCH;
		}
		else
		{
			a = Algorithm.GRASP;
		}
		return a;
	}	

}
