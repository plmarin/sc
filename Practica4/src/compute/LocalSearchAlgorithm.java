package compute;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import reading.FileRead;
import solution.Solution;
import utils.Utils;

/*
 * This class compute a solution from giving solution
 * through some local search heuristics (stipulated in the Algorithm class)
 */
public class LocalSearchAlgorithm implements ComputeAlgorithm 
{
	private final FileRead FILE_READ;
	private Solution solution;

	
	public LocalSearchAlgorithm(ConstructiveAlgorithm constructiveAlgorithm) 
	{
		this(constructiveAlgorithm.getFileRead(),
			constructiveAlgorithm.getSolution());
	}
	
	public LocalSearchAlgorithm(FileRead fileRead, Solution solution) 
	{
		this.FILE_READ = fileRead;
		this.solution = new Solution(solution);
	}
	
	// Compute Algorithm
	@Override
	public void compute(Algorithm algorithm)
	{
		if(algorithm.name().equals(Algorithm.BEST_IMPROVEMENT.name()))
		{
			this.bestImprovement();
		}
		else if(algorithm.name().equalsIgnoreCase(Algorithm.FIRST_LEXICOGRAPHICAL_IMPROVEMENT.name()))
		{
			this.firstLexicographicalImprovement();
		}
		else
		{
			this.firstRandomImprovement();
		}
	}
	
	/*
	 * FIRST RANDOM IMPROVEMENT
	 */
	private void firstRandomImprovement() 
	{
		Integer[] oldSolutionPoints;
		Set<Integer> selectedPoints = new HashSet<Integer>();
		Random rnd = new Random(this.FILE_READ.getChoosen());
		float bestSolution = this.solution.minValue();
		float minValue;
		int solutionPointsSize = this.solution.size();
		int fileReadSize = this.FILE_READ.getSize();
		int solutionPoint;
		boolean improve;
		
		// Iterar hasta que no haya mejora
		do
		{
			improve = false;
			oldSolutionPoints = this.solution.getPoints().toArray(new Integer[this.solution.size()]);
			for(int i = 0; i < solutionPointsSize; i++)
			{
				do
				{
					solutionPoint = (int) oldSolutionPoints[rnd.nextInt(oldSolutionPoints.length)];
				}while(selectedPoints.contains(solutionPoint));
				selectedPoints.add(solutionPoint);
				
				this.solution.removePoint(solutionPoint);		
				
				// Busqueda de una solucion mejor a la anterior
				// Se elimina un punto y se prueba el siguiente
				// Se escoge la mejor solucion y se pasa a la siguiente iteracion
				for(int j = 0; j < fileReadSize; j++)
				{
					// Si se va a examinar el punto elegido o un punto que ya esta
					// en la solucion se salta a la siguiente iteracion
					if(j == solutionPoint || this.solution.contains(j))
					{
						continue;
					}
					
					this.solution.addPoint(j);
					minValue = this.solution.minValue();
					
					// Si se obtiene una mejor solucion se acepta
					// y no se comprueban mas posibilidades con la solucion anterior
					if(minValue > bestSolution)
					{
						improve = true;
						bestSolution = minValue;
						break;
					}
					this.solution.removePoint(j);
				}
				if(improve)
				{
					selectedPoints.clear();
					break;
				}
				this.solution.addPoint(solutionPoint);
			}
		}while(improve);
	}

	/*
	 * FIRST LEXICOGRAPHICAL IMPROVEMENT
	 */
	private void firstLexicographicalImprovement() 
	{
		Solution bestSolution = new Solution(this.solution);
		int fileReadSize = this.FILE_READ.getSize();
		boolean improve;
		
		// Iterar hasta que no haya mejora
		do
		{
			improve = false;
			for(int solutionPoint : bestSolution.getPoints())
			{
				this.solution.removePoint(solutionPoint);
				
				// Busqueda de una solucion mejor a la anterior
				// Se elimina un punto y se prueba el siguiente
				// Se escoge la mejor solucion y se pasa a la siguiente iteracion
				for(int i = 0; i < fileReadSize; i++)
				{
					if(i == solutionPoint || this.solution.contains(i))
					{
						continue;
					}
					
					this.solution.addPoint(i);
					if(this.solution.compareTo(bestSolution) == 1)
					{
						improve = true;
						bestSolution = new Solution(this.solution);
						break;
					}
					this.solution.removePoint(i);
				}
				if(improve)
				{
					break;
				}
				this.solution.addPoint(solutionPoint);
			}
		}while(improve);
	}

	/*
	 * BEST IMPROVEMENT
	 */
	private void bestImprovement() 
	{
		Set<Integer> oldSolutionPoints;
		Random rnd = new Random(this.FILE_READ.getChoosen());
		boolean improve;
		int compare;
		List<Solution> bestSolutions = new ArrayList<Solution>();
		
		// Iterar hasta que no haya mejora
		do
		{
			oldSolutionPoints = new HashSet<Integer>(this.solution.getPoints());
			for(int solutionPoint : oldSolutionPoints)
			{
				Solution bestSolution = new Solution(this.solution);
				bestSolutions.add(bestSolution);
				this.solution.removePoint(solutionPoint);
				improve = false;
				
				// Busqueda de una solucion mejor a la anterior
				// Se elimina un punto y se prueba el siguiente
				// Se guardan todas las posibles soluciones en el Map: bestSolutions
				for(int i = 0; i < this.FILE_READ.getSize(); i++)
				{
					if(i == solutionPoint || this.solution.contains(i))
					{
						continue;
					}
					
					this.solution.addPoint(i);
					compare = this.solution.compareTo(bestSolution);
					if(compare == 1)
					{
						improve = true;
						bestSolutions.clear();
						bestSolution = new Solution(this.solution);
						bestSolutions.add(bestSolution);
					}
					else if(compare == 0)
					{
						bestSolutions.add(new Solution(this.solution));
					}
					this.solution.removePoint(i);
				}
				this.solution.addPoint(solutionPoint);
				
				// Si hay varias "best solutions" se escoge una al azar
				// siempre que se haya mejorado la solucion anterior
				if(bestSolutions.size() > 1 && improve)
				{
					int aux = rnd.nextInt(bestSolutions.size());
					this.solution = (Solution) bestSolutions.toArray()[aux];
				}
				bestSolutions.clear();
			}
		}while(! oldSolutionPoints.containsAll(this.solution.getPoints()));
	}

	public FileRead getFileRead() 
	{
		return this.FILE_READ;
	}

	@Override
	public Solution getSolution() 
	{
		return this.solution;
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[absolutePathFileRead= \"" + this.FILE_READ.getAbosultePathFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("fileRead= \"" + this.FILE_READ.getFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("solution= " + this.solution);
		sb.append("," + System.lineSeparator());	
		sb.append("Objetive Function= " + Utils.roundToNDecimals(this.solution.minValue(), 2) + "]");
		return sb.toString();
	}

}
