package utils;

public class Utils 
{
	public static float roundToNDecimals(float number, int decimals)
	{
		return new Float(Math.round(number * Math.pow(10, decimals)) / Math.pow(10, decimals)).floatValue();
	}
}
