package main;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import reading.FileRead;
import writing.FileWrite;

import compute.Algorithm;
import compute.ComputeSolution;

public class Main 
{
	private static Algorithm algorithm = Algorithm.DESTRUCTIVE_VORACIOUS;
	private static final boolean writeFile = false;
	
	
	public static void main(String[] args) 
	{
		System.err.println("PRÁCTICA 2" + System.lineSeparator() + System.lineSeparator());
		long time_start = System.currentTimeMillis();
		Path path;
		
		if(args.length == 0)
		{
			String separator = System.getProperty("file.separator");
	        //path = Paths.get(".." + separator + "Instancias" + separator + "GKD-Ia" + separator);
	        path = Paths.get(".." + separator + "Instancias" + separator + "04_MMDP_GKD-Ic_1_10" + separator);
		}
		else
		{
			path = Paths.get(args[0]);
			if(args.length > 1)
			{
				Main.algorithm = Main.setAlgorithm(args[1]);
			}
		}
		
		File file = new File(path.toString());
		if(file.isDirectory())
		{
			for(File f : file.listFiles())
			{
				Main.compute(f);
				break;
			}
		}
		else
		{
			Main.compute(file);
		}
		
		long time_end = System.currentTimeMillis();
		double total_time = (time_end - time_start) / 1000d;
		System.err.println("The task has taken " + total_time + " seconds (" + ComputeSolution.getIterations() + " iterations)");		
	}

	private static void compute(File f)
	{
		String fileStr = f.getAbsolutePath();
		FileRead fr = new FileRead(fileStr);
		ComputeSolution[] css = new ComputeSolution[ComputeSolution.getIterations()];
		for(int i = 0; i < ComputeSolution.getIterations(); i++)
		{
			css[i] = new ComputeSolution(fr);
			css[i].compute(Main.algorithm);
		}
		
		String bestSolution = Main.getBestSolution(css);
		if(Main.writeFile)
		{
			FileWrite.writeFile(Main.algorithm.toString(), bestSolution);
		}
		
		System.out.println("Mejor solución: " + bestSolution);
	}

	private static Algorithm setAlgorithm(String algorithm)
	{
		Algorithm a;
		if(algorithm.equalsIgnoreCase(Algorithm.VORACIOUS.name()))
		{
			a = Algorithm.VORACIOUS;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.VORACIOUS_MULTIBOOT.name()))
		{
			a = Algorithm.VORACIOUS;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.DESTRUCTIVE_VORACIOUS.name()))
		{
			a = Algorithm.VORACIOUS;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.DESTRUCTIVE_VORACIOUS_MULTIBOOT.name()))
		{
			a = Algorithm.VORACIOUS;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.DESCOMPOSITION.name()))
		{
			a = Algorithm.VORACIOUS;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.REDUCTION.name()))
		{
			a = Algorithm.VORACIOUS;
		}
		else
		{
			a = Algorithm.RANDOM;
		}
		return a;
	}

	private static String getBestSolution(ComputeSolution[] css) 
	{
		ComputeSolution cs = css[0];
		for(int i = 0; i < css.length; i++)
		{
			cs = cs.getSolution().minValue().compareTo(css[i].getSolution().minValue()) < 0 ? css[i] : cs;
		}
		return cs.toString();
	}

}
