DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_17_n500_m50.txt", 
fileRead= "GKD-Ic_17_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 15, 17, 16, 21, 20, 25, 24, 27, 29, 31, 30, 37, 41, 44, 45, 49, 48, 57, 63, 62, 68, 71, 73, 80, 117, 121, 138, 140, 143, 142, 130, 133, 158, 280, 284, 293, 298],
Objetive Function= 7.98]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_17_n500_m50.txt", 
fileRead= "GKD-Ic_17_n500_m50.txt", 
solution= [275, 138, 142, 10, 132, 284, 21, 20, 23, 158, 24, 146, 27, 265, 29, 31, 169, 443, 36, 312, 41, 167, 44, 50, 293, 178, 298, 63, 303, 476, 65, 202, 349, 86, 81, 80, 90, 238, 101, 370, 230, 227, 225, 379, 117, 354, 484, 247, 121, 120],
Objetive Function= 9.87]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_17_n500_m50.txt", 
fileRead= "GKD-Ic_17_n500_m50.txt", 
solution= [137, 277, 142, 283, 8, 10, 11, 14, 16, 20, 23, 144, 24, 385, 34, 306, 308, 312, 41, 167, 55, 293, 189, 59, 56, 183, 303, 68, 69, 206, 65, 473, 77, 221, 80, 92, 238, 100, 234, 370, 111, 109, 379, 225, 224, 119, 255, 359, 494, 249],
Objetive Function= 9.98]

The task has taken 20.319 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_7_n500_m50.txt", 
fileRead= "GKD-Ic_7_n500_m50.txt", 
solution= [0, 1, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 18, 21, 23, 22, 25, 24, 27, 26, 35, 33, 37, 40, 41, 44, 51, 48, 60, 68, 69, 67, 76, 79, 74, 99, 118, 120, 128, 167, 191, 189, 208, 237, 285, 257, 271, 297],
Objetive Function= 7.88]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_7_n500_m50.txt", 
fileRead= "GKD-Ic_7_n500_m50.txt", 
solution= [0, 139, 273, 3, 128, 132, 14, 256, 21, 23, 146, 27, 270, 29, 271, 31, 35, 306, 174, 40, 41, 167, 288, 191, 294, 189, 60, 71, 338, 67, 348, 79, 223, 457, 456, 95, 450, 331, 211, 102, 103, 237, 229, 227, 492, 118, 117, 116, 251, 120],
Objetive Function= 9.93]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_7_n500_m50.txt", 
fileRead= "GKD-Ic_7_n500_m50.txt", 
solution= [0, 139, 5, 6, 412, 128, 407, 132, 285, 14, 256, 392, 23, 27, 146, 29, 390, 35, 306, 309, 312, 40, 167, 44, 288, 191, 188, 294, 189, 59, 60, 207, 64, 338, 66, 202, 79, 221, 223, 456, 93, 335, 95, 102, 237, 229, 496, 118, 251, 125],
Objetive Function= 10.1]

The task has taken 16.622 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_12_n500_m50.txt", 
fileRead= "GKD-Ic_12_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 8, 11, 12, 13, 14, 15, 16, 19, 23, 24, 27, 26, 28, 35, 32, 36, 37, 42, 44, 50, 54, 56, 78, 79, 83, 106, 107, 104, 116, 115, 124, 138, 142, 129, 156, 163, 185, 205, 197, 192, 258, 308, 337],
Objetive Function= 7.93]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_12_n500_m50.txt", 
fileRead= "GKD-Ic_12_n500_m50.txt", 
solution= [0, 1, 3, 6, 142, 7, 8, 129, 131, 14, 15, 17, 258, 155, 156, 23, 159, 145, 26, 29, 28, 34, 35, 32, 308, 36, 163, 45, 50, 185, 48, 54, 205, 71, 202, 76, 77, 78, 79, 192, 80, 83, 92, 101, 109, 104, 116, 112, 126, 124],
Objetive Function= 9.39]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_12_n500_m50.txt", 
fileRead= "GKD-Ic_12_n500_m50.txt", 
solution= [274, 141, 142, 7, 131, 14, 258, 19, 156, 23, 145, 147, 31, 441, 172, 313, 314, 317, 288, 50, 48, 55, 417, 61, 60, 205, 71, 76, 77, 78, 79, 467, 192, 85, 220, 325, 458, 83, 95, 237, 369, 232, 107, 253, 116, 112, 247, 125, 124, 483],
Objetive Function= 10.23]

The task has taken 15.862 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_15_n500_m50.txt", 
fileRead= "GKD-Ic_15_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 8, 10, 12, 13, 17, 18, 21, 20, 23, 25, 28, 35, 42, 43, 40, 41, 46, 51, 49, 55, 53, 62, 68, 70, 64, 76, 85, 80, 89, 100, 119, 124, 122, 120, 150, 181, 206, 213, 254, 250, 248, 306, 351],
Objetive Function= 7.9]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_15_n500_m50.txt", 
fileRead= "GKD-Ic_15_n500_m50.txt", 
solution= [0, 1, 3, 4, 6, 10, 133, 12, 13, 152, 153, 21, 20, 23, 27, 147, 28, 149, 150, 34, 33, 306, 39, 42, 43, 41, 46, 51, 55, 181, 68, 70, 206, 64, 65, 76, 74, 84, 80, 82, 213, 92, 89, 90, 98, 108, 254, 248, 124, 120],
Objetive Function= 8.86]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_15_n500_m50.txt", 
fileRead= "GKD-Ic_15_n500_m50.txt", 
solution= [275, 137, 4, 276, 413, 10, 133, 12, 13, 153, 16, 257, 23, 22, 150, 34, 306, 39, 172, 42, 43, 425, 424, 50, 290, 55, 417, 179, 206, 340, 64, 348, 72, 75, 87, 86, 215, 89, 210, 98, 108, 379, 254, 248, 247, 244, 367, 360, 122, 120],
Objetive Function= 10.22]

The task has taken 11.215 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_19_n500_m50.txt", 
fileRead= "GKD-Ic_19_n500_m50.txt", 
solution= [0, 1, 2, 3, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 17, 19, 21, 24, 27, 26, 29, 31, 34, 32, 33, 42, 41, 45, 51, 54, 52, 58, 79, 72, 74, 85, 91, 105, 133, 155, 157, 144, 174, 172, 178, 219, 224, 251, 397, 441],
Objetive Function= 8.08]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_19_n500_m50.txt", 
fileRead= "GKD-Ic_19_n500_m50.txt", 
solution= [275, 139, 3, 142, 11, 280, 13, 152, 153, 263, 397, 144, 264, 146, 26, 29, 271, 389, 34, 441, 306, 446, 37, 172, 162, 312, 41, 51, 52, 178, 63, 69, 201, 338, 85, 87, 82, 215, 101, 371, 96, 370, 110, 497, 254, 250, 251, 249, 486, 481],
Objetive Function= 9.89]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_19_n500_m50.txt", 
fileRead= "GKD-Ic_19_n500_m50.txt", 
solution= [139, 3, 140, 282, 280, 12, 13, 153, 392, 263, 157, 397, 144, 386, 271, 389, 30, 388, 441, 306, 444, 163, 438, 427, 178, 421, 69, 201, 338, 200, 78, 79, 467, 85, 87, 83, 215, 329, 371, 370, 110, 229, 497, 104, 492, 250, 353, 491, 249, 486],
Objetive Function= 10.23]

The task has taken 20.896 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_9_n500_m50.txt", 
fileRead= "GKD-Ic_9_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 7, 9, 11, 13, 14, 15, 16, 19, 18, 21, 20, 22, 24, 29, 32, 33, 39, 36, 37, 40, 41, 48, 53, 52, 58, 60, 69, 64, 79, 73, 74, 75, 80, 83, 82, 116, 155, 156, 178, 253, 291, 337, 443],
Objetive Function= 7.97]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_9_n500_m50.txt", 
fileRead= "GKD-Ic_9_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 7, 9, 11, 13, 14, 15, 16, 19, 155, 21, 156, 20, 22, 24, 29, 30, 32, 33, 443, 39, 36, 37, 40, 41, 291, 48, 53, 52, 178, 58, 60, 69, 64, 337, 79, 73, 74, 75, 80, 83, 82, 253, 116],
Objetive Function= 8.03]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_9_n500_m50.txt", 
fileRead= "GKD-Ic_9_n500_m50.txt", 
solution= [0, 411, 6, 276, 282, 280, 401, 132, 14, 16, 393, 21, 157, 265, 271, 173, 312, 318, 425, 291, 292, 191, 430, 178, 296, 298, 177, 302, 181, 68, 343, 476, 64, 337, 78, 193, 461, 218, 103, 235, 111, 230, 228, 104, 224, 493, 118, 495, 494, 366],
Objetive Function= 10.17]

The task has taken 13.374 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_14_n500_m50.txt", 
fileRead= "GKD-Ic_14_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 21, 23, 22, 25, 24, 27, 28, 31, 34, 35, 33, 43, 41, 54, 57, 62, 61, 68, 85, 81, 90, 98, 106, 115, 113, 123, 137, 129, 151, 170, 168, 161, 290, 381],
Objetive Function= 8.03]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_14_n500_m50.txt", 
fileRead= "GKD-Ic_14_n500_m50.txt", 
solution= [137, 2, 5, 6, 7, 129, 9, 10, 11, 13, 15, 16, 21, 23, 22, 25, 24, 28, 31, 151, 34, 170, 33, 168, 43, 40, 161, 41, 44, 290, 54, 53, 57, 62, 68, 73, 85, 87, 81, 83, 91, 90, 98, 381, 106, 105, 115, 113, 124, 123],
Objetive Function= 8.61]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_14_n500_m50.txt", 
fileRead= "GKD-Ic_14_n500_m50.txt", 
solution= [137, 2, 415, 402, 8, 129, 11, 12, 132, 13, 135, 154, 392, 149, 31, 170, 168, 309, 315, 41, 167, 290, 179, 57, 62, 302, 70, 207, 476, 477, 470, 84, 81, 218, 94, 209, 90, 211, 372, 236, 98, 381, 231, 228, 376, 226, 252, 115, 491, 355],
Objetive Function= 10.03]

The task has taken 14.422 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_18_n500_m50.txt", 
fileRead= "GKD-Ic_18_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 14, 17, 16, 19, 18, 21, 23, 22, 25, 27, 26, 28, 31, 35, 32, 38, 39, 36, 37, 50, 53, 59, 63, 62, 61, 80, 91, 98, 96, 97, 108, 124, 206, 216, 228, 244, 262, 498],
Objetive Function= 7.99]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_18_n500_m50.txt", 
fileRead= "GKD-Ic_18_n500_m50.txt", 
solution= [137, 0, 3, 4, 142, 7, 129, 8, 10, 11, 14, 17, 152, 16, 19, 262, 22, 25, 149, 35, 32, 39, 173, 40, 165, 164, 48, 59, 62, 61, 75, 85, 84, 86, 92, 214, 95, 210, 103, 98, 235, 99, 111, 109, 498, 225, 126, 125, 124, 240],
Objetive Function= 9.28]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_18_n500_m50.txt", 
fileRead= "GKD-Ic_18_n500_m50.txt", 
solution= [0, 410, 274, 141, 278, 7, 14, 17, 19, 396, 149, 38, 163, 164, 50, 427, 54, 189, 57, 177, 301, 61, 303, 68, 206, 472, 336, 473, 466, 346, 194, 320, 331, 103, 238, 98, 235, 97, 232, 108, 109, 498, 251, 244, 125, 122, 121, 483, 240, 363],
Objetive Function= 10.22]

The task has taken 13.151 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_13_n500_m50.txt", 
fileRead= "GKD-Ic_13_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 7, 10, 11, 12, 13, 14, 15, 17, 18, 21, 20, 23, 22, 25, 24, 27, 26, 29, 31, 35, 33, 39, 40, 46, 44, 45, 51, 49, 54, 65, 100, 108, 139, 141, 153, 145, 168, 161, 177, 208, 224, 277, 393],
Objetive Function= 7.96]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_13_n500_m50.txt", 
fileRead= "GKD-Ic_13_n500_m50.txt", 
solution= [2, 139, 3, 141, 6, 277, 7, 142, 10, 12, 13, 14, 135, 154, 18, 23, 159, 29, 34, 32, 168, 36, 172, 162, 161, 46, 47, 45, 164, 186, 62, 61, 60, 68, 71, 65, 76, 79, 72, 73, 81, 95, 208, 88, 91, 108, 107, 115, 127, 125],
Objetive Function= 8.95]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_13_n500_m50.txt", 
fileRead= "GKD-Ic_13_n500_m50.txt", 
solution= [137, 139, 3, 414, 412, 6, 8, 13, 135, 17, 393, 266, 145, 26, 269, 171, 304, 445, 310, 432, 162, 161, 435, 46, 51, 185, 55, 419, 183, 478, 338, 65, 337, 464, 465, 463, 326, 327, 460, 212, 208, 230, 108, 229, 109, 227, 497, 225, 488, 243],
Objetive Function= 10.07]

The task has taken 15.237 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_8_n500_m50.txt", 
fileRead= "GKD-Ic_8_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 21, 20, 23, 25, 26, 31, 30, 38, 40, 41, 46, 45, 55, 57, 69, 65, 67, 78, 79, 75, 93, 94, 119, 118, 121, 140, 153, 144, 147, 169, 187, 179, 208, 338, 346],
Objetive Function= 8.08]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_8_n500_m50.txt", 
fileRead= "GKD-Ic_8_n500_m50.txt", 
solution= [2, 140, 8, 400, 11, 12, 13, 152, 154, 263, 20, 25, 386, 264, 385, 147, 384, 31, 269, 304, 38, 187, 55, 59, 179, 176, 63, 180, 69, 206, 338, 66, 67, 76, 350, 79, 346, 448, 237, 110, 111, 225, 379, 254, 357, 116, 250, 491, 123, 361],
Objetive Function= 10.11]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_8_n500_m50.txt", 
fileRead= "GKD-Ic_8_n500_m50.txt", 
solution= [140, 6, 8, 400, 11, 152, 17, 19, 23, 387, 145, 386, 385, 264, 384, 171, 310, 438, 187, 52, 59, 179, 176, 63, 180, 66, 472, 67, 350, 79, 346, 214, 209, 88, 237, 375, 111, 498, 119, 357, 492, 116, 250, 353, 244, 125, 360, 123, 361, 482],
Objetive Function= 10.31]

The task has taken 18.184 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_20_n500_m50.txt", 
fileRead= "GKD-Ic_20_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 17, 16, 21, 22, 24, 26, 29, 28, 31, 34, 35, 32, 37, 43, 53, 52, 60, 68, 69, 70, 76, 72, 85, 80, 90, 100, 110, 108, 109, 120, 253, 241, 262, 290, 354],
Objetive Function= 8.08]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_20_n500_m50.txt", 
fileRead= "GKD-Ic_20_n500_m50.txt", 
solution= [139, 138, 5, 7, 9, 11, 12, 15, 17, 16, 24, 148, 29, 28, 150, 31, 30, 34, 32, 163, 166, 45, 290, 48, 53, 178, 56, 68, 205, 69, 207, 70, 64, 72, 73, 85, 83, 95, 91, 100, 98, 110, 108, 109, 104, 253, 354, 112, 120, 241],
Objetive Function= 9.45]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_20_n500_m50.txt", 
fileRead= "GKD-Ic_20_n500_m50.txt", 
solution= [139, 5, 7, 9, 403, 12, 15, 262, 144, 28, 30, 32, 308, 174, 163, 166, 290, 427, 184, 429, 53, 56, 423, 207, 70, 64, 79, 194, 85, 223, 323, 80, 83, 219, 95, 453, 452, 211, 100, 231, 108, 106, 104, 352, 112, 244, 362, 241, 482, 120],
Objetive Function= 10.24]

The task has taken 15.318 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_16_n500_m50.txt", 
fileRead= "GKD-Ic_16_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 12, 13, 14, 15, 17, 16, 19, 18, 20, 24, 26, 28, 35, 33, 38, 36, 40, 51, 49, 48, 55, 52, 58, 62, 70, 74, 98, 97, 108, 117, 116, 112, 136, 154, 155, 156, 144, 162, 217, 320],
Objetive Function= 8.1]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_16_n500_m50.txt", 
fileRead= "GKD-Ic_16_n500_m50.txt", 
solution= [0, 136, 1, 139, 3, 272, 7, 9, 281, 15, 153, 257, 155, 21, 24, 28, 150, 32, 444, 38, 432, 51, 48, 52, 58, 182, 181, 476, 76, 77, 348, 73, 74, 216, 217, 219, 455, 95, 89, 210, 90, 237, 99, 97, 383, 119, 255, 127, 366, 123],
Objetive Function= 10.0]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_16_n500_m50.txt", 
fileRead= "GKD-Ic_16_n500_m50.txt", 
solution= [0, 139, 141, 142, 8, 9, 11, 133, 394, 155, 21, 20, 396, 144, 28, 268, 389, 151, 33, 308, 45, 48, 55, 52, 58, 298, 182, 62, 181, 303, 342, 479, 70, 72, 195, 74, 326, 320, 89, 233, 111, 108, 107, 119, 250, 113, 112, 355, 246, 482],
Objetive Function= 10.39]

The task has taken 21.882 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_6_n500_m50.txt", 
fileRead= "GKD-Ic_6_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 12, 14, 19, 18, 20, 22, 27, 26, 29, 31, 34, 32, 38, 39, 36, 37, 40, 45, 50, 54, 53, 63, 69, 64, 79, 75, 83, 94, 89, 100, 108, 113, 142, 160, 167, 221, 208, 265, 291, 437],
Objetive Function= 7.96]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_6_n500_m50.txt", 
fileRead= "GKD-Ic_6_n500_m50.txt", 
solution= [137, 0, 275, 138, 3, 277, 400, 133, 286, 14, 405, 153, 18, 20, 25, 267, 265, 26, 31, 34, 32, 306, 39, 40, 319, 165, 426, 185, 54, 56, 71, 349, 79, 75, 463, 83, 457, 94, 330, 208, 331, 102, 100, 99, 356, 254, 118, 250, 360, 121],
Objetive Function= 9.83]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_6_n500_m50.txt", 
fileRead= "GKD-Ic_6_n500_m50.txt", 
solution= [137, 275, 277, 142, 400, 17, 153, 27, 265, 31, 30, 171, 441, 32, 309, 36, 162, 167, 437, 165, 427, 291, 185, 293, 58, 56, 177, 421, 60, 478, 479, 207, 200, 76, 349, 348, 72, 326, 462, 330, 331, 102, 109, 226, 104, 356, 359, 360, 480, 482],
Objetive Function= 10.1]

The task has taken 20.531 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_2_n500_m50.txt", 
fileRead= "GKD-Ic_2_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 12, 13, 15, 16, 19, 20, 25, 26, 28, 32, 33, 39, 36, 37, 43, 41, 46, 44, 45, 51, 53, 59, 56, 60, 69, 82, 95, 90, 100, 124, 145, 173, 172, 185, 188, 200, 237, 246, 264, 444],
Objetive Function= 8.02]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_2_n500_m50.txt", 
fileRead= "GKD-Ic_2_n500_m50.txt", 
solution= [0, 2, 3, 4, 6, 7, 8, 9, 12, 13, 132, 14, 15, 16, 19, 25, 145, 264, 26, 28, 32, 33, 173, 36, 37, 172, 40, 41, 44, 45, 51, 185, 48, 53, 59, 62, 60, 68, 200, 67, 87, 83, 95, 90, 237, 96, 117, 246, 125, 124],
Objetive Function= 8.87]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_2_n500_m50.txt", 
fileRead= "GKD-Ic_2_n500_m50.txt", 
solution= [0, 3, 141, 7, 8, 9, 287, 407, 404, 152, 258, 397, 144, 26, 171, 308, 41, 425, 289, 185, 48, 428, 53, 52, 297, 302, 68, 200, 203, 465, 220, 87, 212, 335, 95, 209, 329, 239, 236, 369, 97, 111, 383, 377, 224, 357, 359, 115, 113, 242],
Objetive Function= 10.25]

The task has taken 14.823 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_5_n500_m50.txt", 
fileRead= "GKD-Ic_5_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 12, 14, 15, 17, 16, 20, 25, 27, 26, 29, 28, 31, 39, 42, 40, 41, 51, 59, 57, 63, 61, 60, 69, 64, 77, 72, 83, 89, 88, 105, 137, 129, 131, 133, 162, 191, 208, 238, 234, 273],
Objetive Function= 8.17]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_5_n500_m50.txt", 
fileRead= "GKD-Ic_5_n500_m50.txt", 
solution= [137, 273, 5, 7, 402, 9, 131, 12, 133, 14, 17, 393, 156, 20, 261, 396, 25, 264, 26, 29, 31, 38, 39, 163, 42, 167, 46, 318, 51, 191, 176, 300, 61, 64, 85, 208, 89, 209, 210, 329, 238, 101, 235, 234, 233, 111, 228, 106, 226, 116],
Objetive Function= 9.7]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_5_n500_m50.txt", 
fileRead= "GKD-Ic_5_n500_m50.txt", 
solution= [137, 0, 1, 141, 402, 400, 14, 257, 20, 386, 26, 28, 31, 170, 32, 307, 33, 39, 173, 310, 172, 42, 432, 162, 40, 167, 316, 424, 191, 179, 57, 183, 61, 60, 64, 77, 79, 466, 85, 94, 208, 209, 102, 101, 234, 233, 377, 378, 244, 125],
Objetive Function= 10.23]

The task has taken 21.015 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_4_n500_m50.txt", 
fileRead= "GKD-Ic_4_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 21, 20, 23, 24, 26, 29, 28, 30, 35, 32, 38, 36, 37, 42, 41, 46, 44, 52, 59, 58, 67, 76, 92, 94, 88, 119, 120, 129, 153, 147, 200, 312, 298],
Objetive Function= 8.09]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_4_n500_m50.txt", 
fileRead= "GKD-Ic_4_n500_m50.txt", 
solution= [273, 6, 277, 8, 11, 12, 284, 153, 19, 23, 386, 26, 148, 151, 30, 440, 170, 32, 41, 47, 166, 186, 51, 291, 431, 52, 59, 298, 343, 342, 201, 339, 76, 471, 73, 221, 460, 216, 219, 92, 214, 94, 88, 106, 104, 493, 119, 117, 251, 242],
Objetive Function= 10.15]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_4_n500_m50.txt", 
fileRead= "GKD-Ic_4_n500_m50.txt", 
solution= [273, 6, 129, 8, 9, 280, 401, 130, 11, 12, 284, 152, 153, 19, 23, 22, 26, 29, 148, 30, 151, 170, 304, 443, 41, 166, 51, 293, 431, 59, 183, 343, 76, 471, 73, 221, 92, 214, 94, 88, 239, 231, 106, 493, 119, 115, 251, 365, 125, 242],
Objetive Function= 10.28]

The task has taken 19.316 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_10_n500_m50.txt", 
fileRead= "GKD-Ic_10_n500_m50.txt", 
solution= [0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 13, 19, 23, 24, 27, 26, 28, 31, 34, 35, 38, 37, 40, 41, 44, 54, 56, 63, 65, 66, 76, 73, 74, 85, 100, 108, 104, 105, 113, 121, 142, 183, 204, 196, 229, 249, 278, 351, 353],
Objetive Function= 8.03]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_10_n500_m50.txt", 
fileRead= "GKD-Ic_10_n500_m50.txt", 
solution= [0, 3, 278, 142, 10, 130, 14, 16, 158, 144, 24, 26, 149, 31, 35, 32, 33, 175, 38, 174, 161, 166, 165, 45, 186, 59, 63, 62, 61, 204, 76, 196, 74, 85, 217, 80, 89, 208, 100, 236, 101, 233, 229, 109, 106, 104, 105, 117, 353, 121],
Objetive Function= 9.46]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_10_n500_m50.txt", 
fileRead= "GKD-Ic_10_n500_m50.txt", 
solution= [408, 2, 279, 5, 278, 130, 19, 23, 266, 264, 391, 33, 312, 161, 166, 165, 164, 186, 55, 293, 188, 59, 296, 58, 343, 205, 479, 70, 351, 76, 196, 193, 72, 74, 323, 208, 100, 374, 371, 229, 108, 109, 227, 496, 106, 104, 105, 353, 490, 127],
Objetive Function= 10.15]

The task has taken 12.975 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_3_n500_m50.txt", 
fileRead= "GKD-Ic_3_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 19, 18, 21, 23, 22, 25, 24, 35, 39, 42, 40, 41, 51, 52, 58, 62, 61, 68, 64, 73, 75, 84, 102, 97, 109, 113, 139, 153, 150, 169, 195, 240, 278, 276, 314, 348],
Objetive Function= 7.93]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_3_n500_m50.txt", 
fileRead= "GKD-Ic_3_n500_m50.txt", 
solution= [408, 278, 142, 8, 281, 131, 393, 157, 267, 146, 147, 26, 148, 149, 35, 38, 37, 314, 51, 48, 293, 294, 58, 63, 62, 423, 60, 64, 67, 77, 467, 73, 195, 75, 465, 84, 86, 218, 209, 369, 368, 99, 234, 228, 224, 252, 353, 126, 240, 363],
Objetive Function= 9.72]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_3_n500_m50.txt", 
fileRead= "GKD-Ic_3_n500_m50.txt", 
solution= [409, 5, 278, 415, 6, 142, 413, 281, 393, 23, 144, 267, 147, 270, 149, 35, 37, 314, 438, 293, 294, 417, 58, 423, 422, 60, 69, 204, 340, 203, 67, 77, 469, 195, 75, 326, 84, 449, 91, 236, 98, 234, 111, 383, 378, 254, 494, 126, 486, 240],
Objetive Function= 10.15]

The task has taken 18.351 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_11_n500_m50.txt", 
fileRead= "GKD-Ic_11_n500_m50.txt", 
solution= [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 16, 19, 18, 20, 25, 28, 31, 32, 33, 39, 36, 37, 40, 50, 48, 54, 53, 52, 61, 72, 80, 91, 109, 119, 141, 169, 185, 191, 197, 198, 236, 286, 427],
Objetive Function= 7.98]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_11_n500_m50.txt", 
fileRead= "GKD-Ic_11_n500_m50.txt", 
solution= [273, 141, 6, 7, 135, 15, 152, 16, 394, 19, 155, 18, 20, 396, 28, 31, 30, 169, 307, 33, 444, 163, 319, 427, 290, 426, 191, 53, 189, 296, 58, 416, 419, 176, 478, 205, 69, 64, 65, 78, 348, 72, 85, 83, 214, 209, 236, 124, 481, 242],
Objetive Function= 9.74]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_11_n500_m50.txt", 
fileRead= "GKD-Ic_11_n500_m50.txt", 
solution= [273, 3, 414, 403, 407, 135, 393, 19, 396, 145, 265, 271, 31, 35, 169, 444, 447, 42, 40, 165, 319, 425, 290, 427, 185, 293, 191, 294, 478, 64, 76, 197, 73, 325, 216, 93, 238, 236, 233, 383, 376, 498, 493, 119, 494, 484, 247, 124, 242, 482],
Objetive Function= 10.24]

The task has taken 15.495 seconds


DESTRUCTIVE_VORACIOUS (1 iterations) + FIRST_RANDOM_IMPROVEMENT + ITERATED_GREEDY algorithms

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_1_n500_m50.txt", 
fileRead= "GKD-Ic_1_n500_m50.txt", 
solution= [0, 1, 2, 3, 5, 6, 8, 12, 13, 17, 16, 18, 23, 22, 25, 24, 27, 30, 34, 35, 32, 38, 37, 42, 43, 40, 44, 51, 50, 48, 56, 63, 69, 65, 66, 75, 87, 81, 93, 97, 110, 111, 105, 121, 136, 154, 175, 247, 295, 449],
Objetive Function= 8.07]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_1_n500_m50.txt", 
fileRead= "GKD-Ic_1_n500_m50.txt", 
solution= [1, 136, 139, 2, 3, 279, 141, 6, 129, 9, 281, 16, 155, 262, 22, 144, 30, 35, 175, 296, 59, 183, 61, 60, 69, 207, 72, 192, 75, 83, 82, 93, 92, 94, 209, 449, 102, 103, 233, 96, 231, 111, 108, 109, 105, 116, 114, 127, 247, 125],
Objetive Function= 9.59]

[absolutePathFileRead= "/home/pablitomarin/Documentos/Universidad/Master/SC/Practicas/Practica4/Instancias_MMDP_GKD-Ic_1_20/GKD-Ic_1_n500_m50.txt", 
fileRead= "GKD-Ic_1_n500_m50.txt", 
solution= [136, 279, 278, 9, 16, 154, 18, 262, 22, 387, 148, 268, 30, 35, 170, 168, 38, 437, 44, 427, 189, 296, 180, 202, 196, 87, 222, 325, 217, 323, 83, 218, 92, 449, 375, 233, 370, 97, 108, 109, 105, 254, 116, 494, 114, 353, 127, 242, 122, 480],
Objetive Function= 10.26]

The task has taken 20.007 seconds


