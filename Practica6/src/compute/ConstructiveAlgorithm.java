package compute;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import reading.FileRead;
import solution.Solution;
import bean.Node;

/*
 * This class compute a solution through some constructive heuristics (stipulated in the Algorithm class)
 */
public class ConstructiveAlgorithm implements ComputeAlgorithm
{	
	private final FileRead FILE_READ;
	private Solution solution;
	private static final int ITERATIONS;
	private static final long[] RAND;
	private static int randCount;
	private static long seed;
	
	
	static
	{
		ITERATIONS = 300;
		RAND = new long[ConstructiveAlgorithm.ITERATIONS];
		for(int i = 0; i < ConstructiveAlgorithm.ITERATIONS; i++)
		{
			ConstructiveAlgorithm.RAND[i] = i;
		}
		ConstructiveAlgorithm.randCount = 0;
	}
	
	public ConstructiveAlgorithm(FileRead fileRead)
	{
		this(fileRead, new Solution(fileRead));
	}
	
	public ConstructiveAlgorithm(FileRead fileRead, Solution solution)
	{
		if(ConstructiveAlgorithm.randCount >= ConstructiveAlgorithm.ITERATIONS)
			ConstructiveAlgorithm.randCount = 0;
		ConstructiveAlgorithm.seed = ConstructiveAlgorithm.RAND[ConstructiveAlgorithm.randCount++];
		this.FILE_READ = fileRead;
		this.solution = solution;
	}
	
	// Compute Algorithm
	@Override
	public void compute(Algorithm algorithm)
	{
		if(algorithm.name().equals(Algorithm.VORACIOUS.name()))
		{
			this.voracious();
		}
		else if(algorithm.name().equals(Algorithm.VORACIOUS_MULTIBOOT.name()))
		{
			this.voraciousMultiboot();
		}
		else if(algorithm.name().equals(Algorithm.RANDOM.name()))
		{
			this.random();
		}
		else
		{
			System.err.println(algorithm.name() + " algorithm hasn't implement yet." +
					" Try with other constructive algorithm");
			System.exit(-1);
		}
	}
	
	/*
	 * RANDOM HEURISTIC
	 */
	private void random()
	{
		Random rnd = new Random(ConstructiveAlgorithm.seed);
		
		int sizeNodeArray = this.solution.getNodeArray().length;
		List<Integer> nodeIndexList = new ArrayList<Integer>(sizeNodeArray);
		for(int i = 0; i < sizeNodeArray; i++)
		{
			nodeIndexList.add(i);
		}
		
		// Se escogen los hubs requeridos al azar
		int index;
		for(int hubs = 0; hubs < this.FILE_READ.getHubs(); hubs++)
		{
			index = rnd.nextInt(nodeIndexList.size());
			int hubIndex = nodeIndexList.remove(index);
			this.solution.addHub(hubIndex);
		}
		
		// Se asocian los nodos restantes a los hubs teniendo en cuenta las restricciones
		int nodeIndex;
		int hubIndex;
		int sizeHubIndexList = this.solution.getHubsAdded();
		int maxIterations = sizeHubIndexList * 2;
		int iterations = 0;
		while(nodeIndexList.size() > 0)
		{
			hubIndex = rnd.nextInt(sizeHubIndexList);
			index = rnd.nextInt(nodeIndexList.size());
			nodeIndex = nodeIndexList.remove(index);
			if(! this.solution.addNode(hubIndex, nodeIndex))
			{
				if(iterations == maxIterations)
				{
					this.discardSolution(this.solution);
					return;
				}
				nodeIndexList.add(nodeIndex);
				iterations++;
			}
			else
			{
				iterations = 0;
			}
		}
	}
	
	private void discardSolution(Solution solution) 
	{
		int sizeHubIndexList = solution.getHubsAdded();
		float[] distances = new float[sizeHubIndexList];
		for(int i = 0; i < distances.length; i++)
		{
			distances[i] = Float.MAX_VALUE / (sizeHubIndexList + 1);
		}
		solution.setDistances(distances);
	}

	/*
	 * VORACIOUS HEURISTIC - Multiboot
	 */
	// Multiboot --> Anadiendo nodos a hubs al azar
	private void voraciousMultiboot()
	{
		// Method 1: Hubs --> Los que mas consumen
		/*
		// Se escogen los nodos que mas consumen como hubs
		int[] hubsIndex = this.chooseBigDemandNodes();
		this.solution.addHubs(hubsIndex);
		*/
		
		// Method 2: Hubs --> Los mas lejanos
		// Se escogen los nodos que mas lejos estan como hubs
		int[] hubsIndex = this.chooseFarNodes();
		
		
		// Se asocian los nodos restantes a los hubs teniendo en cuenta las restricciones
		Random rnd = new Random(ConstructiveAlgorithm.seed);
		Node[] nodes = this.solution.getNodeArray();
		int sizeNodeIndexList = this.solution.getNodeArray().length - hubsIndex.length;
		List<Integer> nodeIndexList = new ArrayList<Integer>(sizeNodeIndexList);
		for(int i = 0; i < sizeNodeIndexList; i++)
		{
			if(! nodes[i].isHub())
			{
				nodeIndexList.add(i);
			}
		}
		
		int hubIndex;
		int nodeIndex;
		int busyNodes = hubsIndex.length;
		int maxIterations = hubsIndex.length * 2;
		int iterations = 0;
		do
		{
			hubIndex = rnd.nextInt(hubsIndex.length);
			nodeIndex = this.solution.nearestNodeIndex(hubIndex);
			if(this.solution.addNode(hubIndex, nodeIndex))
			{
				busyNodes++;
				iterations = 0;
			}
			else
			{
				if(iterations == maxIterations)
				{
					this.discardSolution(this.solution);
					return;
				}
				iterations++;
			}
		}while(busyNodes < nodes.length);
	}
	
	/*
	 * VORACIOUS HEURISTIC
	 */
	private void voracious()
	{
		// Method 1: Hubs --> Los que mas consumen
		/*
		// Se escogen los nodos que mas consumen como hubs
		int[] hubsIndex = this.chooseBigDemandNodes();
		this.solution.addHubs(hubsIndex);
		*/
		
		// Method 2: Hubs --> Los mas lejanos
		// Se escogen los nodos que mas lejos estan como hubs
		int[] hubsIndex = this.chooseFarNodes();
		
		
		// Se asocian los nodos restantes a los hubs teniendo en cuenta las restricciones
		Node[] nodes = this.solution.getNodeArray();
		int sizeNodeIndexList = this.solution.getNodeArray().length - hubsIndex.length;
		List<Integer> nodeIndexList = new ArrayList<Integer>(sizeNodeIndexList);
		for(int i = 0; i < sizeNodeIndexList; i++)
		{
			if(! nodes[i].isHub())
			{
				nodeIndexList.add(i);
			}
		}
		
		int hubIndex;
		int nodeIndex;
		int busyNodes = hubsIndex.length;
		int maxIterations = hubsIndex.length * 2;
		int iterations = 0;
		do
		{
			hubIndex = this.solution.getIndexMinDistance();
			nodeIndex = this.solution.nearestNodeIndex(hubIndex);
			if(this.solution.addNode(hubIndex, nodeIndex))
			{
				busyNodes++;
				iterations = 0;
			}
			else
			{
				if(iterations == maxIterations)
				{
					this.discardSolution(this.solution);
					return;
				}
				iterations++;
			}
		}while(busyNodes < nodes.length);		
	}

	private int[] chooseFarNodes() 
	{
		int[] hubsIndex = new int[this.solution.getHubsIndex().length];
		for(int hubIndex = 0; hubIndex < this.solution.getHubsIndex().length; hubIndex++)
		{
			hubsIndex[hubIndex] = this.solution.getIndexMaxDistanceAllDistances();
			this.solution.addHub(hubIndex);
		}
		return hubsIndex;
	}

	@SuppressWarnings("unused")
	private int[] chooseBigDemandNodes()
	{
		Node[] nodes = this.solution.getNodeArray();
		int[] hubsIndex = new int[this.solution.getHubsIndex().length];
		List<Integer> biggestDemands = new ArrayList<Integer>(this.solution.getHubsIndex().length);
		for(int i = 0; i < this.solution.getHubsIndex().length; i++)
		{
			biggestDemands.add(nodes[i].getDemandValue());
			hubsIndex[i] = i;
		}
		Collections.sort(biggestDemands);

		for(int i = biggestDemands.size() - 1; i < nodes.length; i++)
		{
			for(int j = 0; j < biggestDemands.size(); j++)
			{
				if(nodes[i].getDemandValue() > biggestDemands.get(j))
				{
					biggestDemands.remove(j);
					biggestDemands.add(nodes[i].getDemandValue());
					Collections.sort(biggestDemands);
					break;
				}
			}
		}
		
		int j = 0;
		for(int i = 0; i < nodes.length; i++)
		{
			if(biggestDemands.contains(nodes[i].getDemandValue()))
			{
				hubsIndex[j++] = i;
				biggestDemands.remove(new Integer(nodes[i].getDemandValue()));
			}
		}
		return hubsIndex;
	}

	public final FileRead getFileRead() 
	{
		return this.FILE_READ;
	}

	@Override
	public Solution getSolution() 
	{
		return this.solution;
	}

	public static int getIterations() 
	{
		return ConstructiveAlgorithm.ITERATIONS;
	}

	public static long[] getRand() 
	{
		return ConstructiveAlgorithm.RAND;
	}

	public static int getRandCount() 
	{
		return ConstructiveAlgorithm.randCount;
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[absolutePathFileRead= \"" + this.FILE_READ.getAbosultePathFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("fileRead= \"" + this.FILE_READ.getFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("solution= " + this.solution);
		sb.append("," + System.lineSeparator());	
		sb.append("Objetive Function= " + this.solution.getDistance() + "]");
		return sb.toString();
	}
	
}
