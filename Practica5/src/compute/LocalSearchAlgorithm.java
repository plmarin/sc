package compute;

import java.util.Random;

import reading.FileRead;
import solution.Solution;

/*
 * This class compute a solution from giving solution
 * through some local search heuristics (stipulated in the Algorithm class)
 */
public class LocalSearchAlgorithm implements ComputeAlgorithm 
{
	private final FileRead FILE_READ;
	private Solution solution;

	
	public LocalSearchAlgorithm(ConstructiveAlgorithm constructiveAlgorithm) 
	{
		this(constructiveAlgorithm.getFileRead(),
			constructiveAlgorithm.getSolution());
	}
	
	public LocalSearchAlgorithm(FileRead fileRead, Solution solution) 
	{
		this.FILE_READ = fileRead;
		this.solution = new Solution(solution);
	}
	
	public LocalSearchAlgorithm(FileRead fileRead) 
	{
		this.FILE_READ = fileRead;
		this.solution = null;
	}
	
	// Compute Algorithm
	@Override
	public void compute(Algorithm algorithm)
	{
		if(algorithm.name().equals(Algorithm.BEST_IMPROVEMENT.name()))
		{
			this.bestImprovement();
		}
		else if(algorithm.name().equalsIgnoreCase(Algorithm.FIRST_LEXICOGRAPHICAL_IMPROVEMENT.name()))
		{
			this.firstLexicographicalImprovement();
		}
		else
		{
			this.firstRandomImprovement();
		}
	}
	
	/*
	 * FIRST RANDOM IMPROVEMENT
	 */
	private void firstRandomImprovement() 
	{
		Solution bestSolution = new Solution(this.solution);
		boolean improve;
		int indexListSize = bestSolution.getIndexList().size();
		int maxCut = this.solution.maxCut();
		int bestSolutionMaxCut;
		Random rnd = new Random(indexListSize);
		int iindex;
		int jindex;
		int element;
		
		// Iterate until not find improvement
		do
		{
			iindex = rnd.nextInt(indexListSize - 1);
			improve = false;
			for(int i = iindex; i < (indexListSize - 1) + iindex; i++)
			{
				jindex = i < indexListSize - 1 ? i : i - (indexListSize - 1);
					
				for(int j = jindex; j < (indexListSize - 1) + jindex; j++)
				{
					element = j < indexListSize - 1 ? j : j - (indexListSize - 1);
					bestSolution.moveVertexToNextPosition(element);
					bestSolutionMaxCut = bestSolution.maxCut();
					if(maxCut > bestSolutionMaxCut)
					{
						this.solution = new Solution(bestSolution);
						maxCut = bestSolutionMaxCut;
						improve = true;
						break;
					}
				}
				if(improve)
				{
					break;
				}
				bestSolution = new Solution(this.solution);
			}
		}while(improve);
	}

	/*
	 * FIRST LEXICOGRAPHICAL IMPROVEMENT
	 */
	private void firstLexicographicalImprovement() 
	{
		Solution bestSolution = new Solution(this.solution);
		boolean improve;
		int indexListSize = bestSolution.getIndexList().size();
		int maxCut = this.solution.maxCut();
		int bestSolutionMaxCut;
		
		// Iterate until not find improvement
		do
		{
			improve = false;
			for(int i = 0; i < indexListSize - 1; i++)
			{
				for(int j = i; j < indexListSize - 1; j++)
				{
					bestSolution.moveVertexToNextPosition(j);
					bestSolutionMaxCut = bestSolution.maxCut();
					if(maxCut > bestSolutionMaxCut)
					{
						this.solution = new Solution(bestSolution);
						maxCut = bestSolutionMaxCut;
						improve = true;
						break;
					}
				}
				if(improve)
				{
					break;
				}
				bestSolution = new Solution(this.solution);
			}
		}while(improve);
	}

	/*
	 * BEST IMPROVEMENT
	 */
	private void bestImprovement() 
	{
		System.err.println(Algorithm.BEST_IMPROVEMENT.name() + " algorithm hasn't implement yet." +
				" Try with other local search algorithm");
		System.exit(-1);
	}

	public FileRead getFileRead() 
	{
		return this.FILE_READ;
	}

	@Override
	public Solution getSolution() 
	{
		return this.solution;
	}
	
	public void setSolution(Solution solution) 
	{
		this.solution = solution;
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[absolutePathFileRead= \"" + this.FILE_READ.getAbosultePathFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("fileRead= \"" + this.FILE_READ.getFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("solution= " + this.solution);
		sb.append("," + System.lineSeparator());	
		sb.append("Objetive Function= " + this.solution.maxCut() + "]");
		return sb.toString();
	}

}
