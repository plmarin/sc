package reading;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import utils.Utils;

public class FileRead
{
	private int size;
	private int choosen;
	private float[][] matrixDistances;
	private File file;
	private String fileName;
	private String abosultePathFileName;
	
	
	public FileRead(String absolutePath)
	{
		this.fileName = absolutePath.substring(absolutePath.lastIndexOf(File.separator) + 1);
		this.abosultePathFileName = absolutePath;
		this.file = new File(this.abosultePathFileName);
		FileReader fr = null;
		BufferedReader br = null;
		int pos_x;
		int pos_y;
		float value;
		
		try 
		{
			fr = new FileReader(this.file);
			br = new BufferedReader(fr);

			// File reading
			String line = br.readLine();
			String[] elements = line.split(" ");
			// n
			this.size = Integer.parseInt(elements[0]);
			// m
			this.choosen = Integer.parseInt(elements[1]);
			this.matrixDistances = new float[this.size][this.size];
			
			while((line = br.readLine()) != null)
			{
				elements = line.split(" ");
				pos_x = Integer.parseInt(elements[0]);
				pos_y = Integer.parseInt(elements[1]);
				value = Float.parseFloat(elements[2]);
				this.matrixDistances[pos_x][pos_y] = value;
				this.matrixDistances[pos_y][pos_x] = value;
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			this.closeFile(fr);
		}
	}

	private void closeFile(FileReader fr)
	{
		try 
		{
			if(fr != null) 
			{
				fr.close();
			}
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
	}
	
	public void printMatrixDistances()
	{
		for(int i = 0; i < this.matrixDistances.length; i++)
		{
			for(int j = 0; j < this.matrixDistances[i].length; j++)
			{
				System.out.print(this.matrixDistances[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	public void printAbbreviateMatrixDistances()
	{
		for(int i = 0; i < this.matrixDistances.length; i++)
		{
			for(int j = 0; j < this.matrixDistances[i].length; j++)
			{
				System.out.print(Utils.roundToNDecimals(this.matrixDistances[i][j], 2) + " ");
			}
			System.out.println();
		}
	}
	
	public void printPositionsMatrixDistances()
	{
		for(int i = 0; i < this.matrixDistances.length; i++)
			for(int j = i + 1; j < this.matrixDistances[i].length; j++)
				System.out.println("MatrixDistances[" + i + "][" + j + "] = " + this.matrixDistances[i][j]);
	}
	
	public void printAbbreviatePositionsMatrixDistances()
	{
		for(int i = 0; i < this.matrixDistances.length; i++)
			for(int j = i + 1; j < this.matrixDistances[i].length; j++)
				System.out.println("MatrixDistances[" + i + "][" + j + "] = " + Utils.roundToNDecimals(this.matrixDistances[i][j], 2));
	}

	public int getSize() 
	{
		return this.size;
	}

	public void setSize(int size) 
	{
		this.size = size;
	}

	public int getChoosen() 
	{
		return this.choosen;
	}

	public void setChoosen(int choosen) 
	{
		this.choosen = choosen;
	}

	public float[][] getMatrixDistances() 
	{
		return this.matrixDistances;
	}

	public void setMatrixDistances(float[][] matrixDistances)
	{
		this.matrixDistances = matrixDistances;
	}

	public String getFileName()
	{
		return this.fileName;
	}
	
	public String getAbosultePathFileName()
	{
		return this.abosultePathFileName;
	}

	public float[][] getCopyOfMatrixDistances()
	{
		float[][] copyOfMatrixDistances = new float[this.size][this.size];
		for(int i = 0; i < this.matrixDistances.length; i++)
		{
			System.arraycopy(this.matrixDistances[i], 0, copyOfMatrixDistances[i], 0, this.size);
		}
		return copyOfMatrixDistances;
	}
	
}
