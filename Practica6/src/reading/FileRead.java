package reading;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import utils.Utils;

import bean.Node;

public class FileRead
{
	private int nodes;
	private int hubs;
	private int hubCapacity;
	private Node[] nodeArray;
	private float[][] allDistances;
	private File file;
	private String fileName;
	private String abosultePathFileName;
	
	
	public FileRead(String absolutePath)
	{
		this.fileName = absolutePath.substring(absolutePath.lastIndexOf(File.separator) + 1);
		this.abosultePathFileName = absolutePath;
		this.file = new File(this.abosultePathFileName);
		FileReader fr = null;
		BufferedReader br = null;
		int id;
		int pos_x;
		int pos_y;
		int demandValue;
		
		try 
		{
			fr = new FileReader(this.file);
			br = new BufferedReader(fr);

			// File reading
			String line = br.readLine();
			String[] elements = line.split(" ");
			// Nodes
			this.nodes = Integer.parseInt(elements[0]);
			// Hubs
			this.hubs = Integer.parseInt(elements[1]);
			// Hub capacity
			this.hubCapacity = Integer.parseInt(elements[2]);
			// Store information
			this.nodeArray = new Node[this.nodes];
			
			while((line = br.readLine()) != null)
			{
				elements = line.split(" ");
				id = Integer.parseInt(elements[0]);
				pos_x = Integer.parseInt(elements[1]);
				pos_y = Integer.parseInt(elements[2]);
				demandValue = Integer.parseInt(elements[3]);
				// Id of node start in 1 (not in 0) --> Avoid ArrayOutOfBounds
				this.nodeArray[id - 1] = new Node(id, pos_x, pos_y, demandValue, this.hubCapacity);
			}
			
			//Store all distances
			this.allDistances = new float[this.nodes][this.nodes];
			this.fillDistances();
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			this.closeFile(fr);
		}
	}
	
	private void fillDistances() 
	{
		for(int i = 0; i < this.nodes; i++)
		{
			for(int j = i; j < this.nodes; j++)
			{
				if(i != j)
				{
					this.allDistances[i][j] = this.allDistances[j][i] = Utils.euclideanDistance(this.nodeArray[i].getPoint(), this.nodeArray[j].getPoint());
				}					
				else
				{
					this.allDistances[i][j] = 0.0f;
				}
			}
		}		
	}

	private void closeFile(FileReader fr)
	{
		try 
		{
			if(fr != null) 
			{
				fr.close();
			}
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
	}
	
	public void printNodeArray()
	{
		for(int i = 0; i < this.nodeArray.length; i++)
		{
			System.out.println(this.nodeArray[i]);
		}
	}
	
	public void printPositionsNodeArray()
	{
		for(int i = 0; i < this.nodeArray.length; i++)
			System.out.println("NodeArray[" + i + "] = " + this.nodeArray[i]);
	}

	public int getNodes() 
	{
		return this.nodes;
	}

	public void setNodes(int nodes) 
	{
		this.nodes = nodes;
	}

	public int getHubs() 
	{
		return this.hubs;
	}

	public void setHubs(int hubs) 
	{
		this.hubs = hubs;
	}

	public int getHubCapacity() 
	{
		return this.hubCapacity;
	}

	public void setHubCapacity(int hubCapacity) 
	{
		this.hubCapacity = hubCapacity;
	}

	public Node[] getNodeArray() 
	{
		return this.nodeArray;
	}

	public void setNodeArray(Node[] nodeArray)
	{
		this.nodeArray = nodeArray;
	}

	public File getFile()
	{
		return this.file;
	}

	public String getFileName()
	{
		return this.fileName;
	}
	
	public String getAbosultePathFileName()
	{
		return this.abosultePathFileName;
	}

	public float[][] getAllDistances() 
	{
		return this.allDistances;
	}
	
	public void printAllDistances()
	{
		for(int i = 0; i < this.allDistances.length; i++)
		{
			for(int j = 0; j < this.allDistances[i].length; j++)
			{
				System.out.print(this.allDistances[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	public void printPositionsAllDistances()
	{
		for(int i = 0; i < this.allDistances.length; i++)
			for(int j = i + 1; j < this.allDistances[i].length; j++)
				System.out.println("AllDistances[" + (i + 1) + "][" + (j + 1) + "] = " + this.allDistances[i][j]);
	}
	
	public float[][] getCopyOfAllDistances() 
	{
		float[][] copyOfAllDistances = new float[this.nodes][this.nodes];
		for(int i = 0; i < this.nodes; i++)
		{
			System.arraycopy(this.nodeArray[i], 0, copyOfAllDistances[i], 0, this.nodes);
		}
		return copyOfAllDistances;
	}

	public Node[] getCopyOfNodeArray()
	{
		Node[] copyOfNodeArray = new Node[this.nodes];
		for(int i = 0; i < this.nodes; i++)
		{
			copyOfNodeArray[i] = new Node(this.nodeArray[i]);
		}
		return copyOfNodeArray;
	}
	
}
