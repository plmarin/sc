package compute;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import reading.FileRead;
import solution.Solution;
import utils.Utils;

/*
 * This class compute a solution from giving solution
 * through some metaheuristics (stipulated in the Algorithm class)
 */
public class MetaheuristicAlgorithm implements ComputeAlgorithm
{
	private final long TOTAL_TIME;
	private static final int TOTAL_ITERATIONS;
	private static final int DESTROY_ITERATIONS;
	private final long INITIAL_TIME;
	private int iterations;
	private final FileRead FILE_READ;
	private Solution solution;
	private Algorithm algorithm;
	
	
	static
	{
		TOTAL_ITERATIONS = 7; // Stop condition
		DESTROY_ITERATIONS = 7; // Number of points destroyed in each greedy iteration 
	}

	public MetaheuristicAlgorithm(long time, LocalSearchAlgorithm localSearchAlgorithm, long totalTime) 
	{
		this(time,
			localSearchAlgorithm.getFileRead(),
			localSearchAlgorithm.getSolution(),
			totalTime);	
	}
	
	public MetaheuristicAlgorithm(long time, FileRead fileRead, Solution solution, long totalTime) 
	{
		this.INITIAL_TIME = time;
		this.FILE_READ = fileRead;
		this.solution = new Solution(solution);
		this.iterations = 0;
		this.TOTAL_TIME = totalTime;
	}
	
	// Compute Algorithm
	@Override
	public void compute(Algorithm algorithm)
	{
		this.algorithm = algorithm;
		if(this.algorithm.name().equalsIgnoreCase(Algorithm.ITERATED_GREEDY.name()))
		{
			this.iteratedGreedy();
		}
		else
		{
			System.err.println(algorithm.name() + " algorithm hasn't implement yet." +
					" Try with other constructive algorithm");
			System.exit(-1);
		}
	}

	/*
	 * ITERATED GREEDY
	 * 
	 * 1. procedimiento IG
	 * 2. 	x <-- ConstruirSolucionInicial();
	 * 3. 	x' <-- BusquedaLocal(x);
	 * 4.	mientras (not CriterioParada) hacer
	 * 5.		solParcial <-- DestruirSolucion(x');
	 * 6.		x'' <-- ConstruirSolucion(solParcial);
	 * 7.		x' <-- CriterioAceptacion(x'', x');
	 * 8.	fin mientras;
	 * 9.	devolver x';
	 * 10. fin IG;
	 */
	private void iteratedGreedy() 
	{
		Solution bestSolution = new Solution(this.solution);
		do
		{
			bestSolution = this.destroySolution(bestSolution);
			bestSolution = this.buildSolution(bestSolution);
			if(bestSolution.compareTo(this.solution) == 1)
			{
				this.solution = new Solution(bestSolution);
			}
			else
			{
				bestSolution = new Solution(this.solution);
			}
		}while(! this.stopCondition());
	}
	
	private Solution destroySolution(Solution bestSolution) 
	{
		Set<Integer> deletedSolutionPoints = new HashSet<Integer>();
		Random rnd = new Random(this.iterations);
		Integer[] solutionPoints = this.solution.getPoints().toArray(new Integer[this.solution.size()]);
		int percentage = (this.solution.size() * 80) / 100;
		int point;
		for(int i = 0; i < MetaheuristicAlgorithm.DESTROY_ITERATIONS; i++)
		{
			// Hacer mientras el punto haya sido elegido anteriormente o
			// mientras el 80% de los puntos no hayan sido seleccionados
			do
			{
				point = solutionPoints[rnd.nextInt(this.solution.size())];
			}while(deletedSolutionPoints.contains(point) && deletedSolutionPoints.size() < percentage);
		
			deletedSolutionPoints.add(point);
			bestSolution.removePoint(point);
		}
		
		return bestSolution;
	}
	
	private Solution buildSolution(Solution bestSolution)
	{
		ConstructiveAlgorithm ca = new ConstructiveAlgorithm(this.FILE_READ, bestSolution);
		ca.compute(Algorithm.VORACIOUS);
		LocalSearchAlgorithm lsa = new LocalSearchAlgorithm(ca);
		lsa.compute(Algorithm.FIRST_RANDOM_IMPROVEMENT);
		return lsa.getSolution();
	}

	private boolean stopCondition() 
	{
		boolean stopCondition;
		
		// Tiempo
		stopCondition = System.currentTimeMillis() - this.INITIAL_TIME > this.TOTAL_TIME;
		// Numero iteraciones
		stopCondition = stopCondition || (++this.iterations > MetaheuristicAlgorithm.TOTAL_ITERATIONS);

		return stopCondition;
	}

	public FileRead getFileRead() 
	{
		return this.FILE_READ;
	}
	
	@Override
	public Solution getSolution() 
	{
		return this.solution;
	}
	
	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[absolutePathFileRead= \"" + this.FILE_READ.getAbosultePathFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("fileRead= \"" + this.FILE_READ.getFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("solution= " + this.solution);
		sb.append("," + System.lineSeparator());	
		sb.append("Objetive Function= " + Utils.roundToNDecimals(this.solution.minValue(), 2) + "]");
		return sb.toString();
	}

}
