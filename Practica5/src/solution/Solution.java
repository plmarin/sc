package solution;

import java.util.ArrayList;
import java.util.List;

import reading.FileRead;

public class Solution implements Comparable<Solution>
{
	private boolean[][] adjacentMatrix;
	private List<List<Integer>> adjacentList;
	private List<Integer> indexList;
	private int[] cuts;
	
	
	public Solution(FileRead fileRead)
	{
		this.adjacentMatrix = fileRead.getAdjacentMatrix();
		this.adjacentList = fileRead.getAdjacentList();
		this.indexList = new ArrayList<Integer>(this.adjacentList.size());
		for(int i = 0; i < this.adjacentList.size(); i++)
		{
			this.indexList.add(i);
		}
		this.cuts = new int[this.adjacentList.size() - 1];  
		this.computeCuts();
	}
	
	public Solution(Solution solution) 
	{
		this.adjacentMatrix = solution.getAdjacentMatrix();
		this.adjacentList = solution.getAdjacentList();
		this.indexList = new ArrayList<Integer>(solution.getIndexList());
		this.cuts = new int[this.adjacentList.size() - 1];
		System.arraycopy(solution.getCuts(), 0, this.cuts, 0, solution.getCuts().length);
	}
	
	public int minCut()
	{		
		int minCut = Integer.MAX_VALUE;
		for(int cut = 0; cut < this.cuts.length; cut++)
		{
			minCut = minCut > this.cuts[cut] ? this.cuts[cut] : minCut;
		}
		return minCut;
	}
	
	// Objective function
	public int maxCut()
	{		
		int maxCut = 0;
		for(int cut = 0; cut < this.cuts.length; cut++)
		{
			maxCut = maxCut < this.cuts[cut] ? this.cuts[cut] : maxCut;
		}
		return maxCut;
	}
	
	public void moveVertexToNextPosition(int vertex)
	{
		int element = this.indexList.remove(vertex);
		if(vertex == this.cuts.length - 1)
		{
			this.indexList.add(0, element);
			this.computeCuts();
		}
		else
		{
			this.indexList.add(vertex + 1, element);
			this.computeCuts(vertex, vertex + 1);
		}
	}
	
	public void computeCuts()
	{
		this.computeCuts(0, this.cuts.length);
	}

	public void computeCuts(int startPos, int endPos) 
	{
		// startPos = 0 & endPos = cuts.length:
		// for(int cut = 0; cut < cuts.length; cut++)
		for(int cut = startPos; cut < endPos; cut++)
		{
			this.cuts[cut] = 0;
			for(int i = 0; i <= cut; i++)
			{
				for(int j = cut + 1; j < this.adjacentMatrix[i].length; j++)
				{
					if(this.adjacentMatrix[this.indexList.get(i)][this.indexList.get(j)])
					{
						this.cuts[cut]++;
					}
				}
			}
		}	
	}
	
	public boolean[][] getAdjacentMatrix() 
	{
		return this.adjacentMatrix;
	}

	public void setAdjacentMatrix(boolean[][] adjacentMatrix)
	{
		this.adjacentMatrix = adjacentMatrix;
	}
	
	public List<List<Integer>> getAdjacentList()
	{
		return this.adjacentList;
	}

	public void setAdjacentList(List<List<Integer>> adjacentList)
	{
		this.adjacentList = adjacentList;
	}

	public List<Integer> getIndexList()
	{
		return this.indexList;
	}

	public void setIndexList(List<Integer> index) 
	{
		this.indexList = index;
	}
	
	public int[] getCuts()
	{
		return this.cuts;
	}

	public void setCuts(int[] cuts)
	{
		this.cuts = cuts;
	}

	@Override
	public int compareTo(Solution s)
	{
		int max = this.maxCut();
		float sMax = s.maxCut();
		return max == sMax ? 0 : (max < sMax ? -1 : 1);
	}
	
	@Override
	public String toString()
	{
		return this.indexList.toString();
	}

}
