package main;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import reading.FileRead;
import writing.FileWrite;

import compute.Algorithm;
import compute.ConstructiveAlgorithm;
import compute.LocalSearchAlgorithm;
import compute.MetaheuristicAlgorithm;

public class Main
{
	private static Algorithm constructiveAlgorithm = Algorithm.DESTRUCTIVE_VORACIOUS;
	private static Algorithm localSearchAlgorithm = Algorithm.FIRST_RANDOM_IMPROVEMENT;
	private static Algorithm metaheuristicAlgorithm = Algorithm.ITERATED_GREEDY;
	private static boolean writeFile = true;
	private static final String SEPARATOR = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"; 
	
	
	public static void main(String[] args) 
	{
		Path path = Main.getPath(args);
		
		System.out.println("PRÁCTICA 4_OLD - OBLIGATORIA 1");
		System.out.println("Heurísticas: " +
				Main.constructiveAlgorithm + " (constructivo) + " + 
				Main.localSearchAlgorithm + " (búsqueda local)" + System.lineSeparator() +
				"Metaheurística: " + Main.metaheuristicAlgorithm + System.lineSeparator());
		
		File file = path.toFile();
		if(! file.exists())
		{
			//Si no existe la ruta especificada se le asigna la instancia de las practicas por defecto
			String separator = System.getProperty("file.separator");
			String defaultPath = "Instancias_MMDP_GKD-Ic_1_20" + separator; 
			file = new File(defaultPath);
			
			//Si sigue sin existir se sale del programa con la notificacion del error
			if(! file.exists())
			{
				System.err.println("No se pudo acceder a la ruta: " + System.lineSeparator() +
						" \"" + path.toAbsolutePath().normalize().toString() + "\"." + System.lineSeparator() +
						"Tampoco se pudo acceder a la ruta por defecto: " + System.lineSeparator() +
						" \"" + Paths.get(defaultPath).toAbsolutePath().toString() + "\".");
				System.exit(-1);
			}
		}
		
		//Inicio computo
		final long timeStart;
		final long timeEnd;
		final double totalTime;
		timeStart = System.currentTimeMillis();
		
		Main.init(file);
		//Main.compute(Paths.get(file.toString() + "/GKD-Ic_1_n500_m50.txt").toFile());
		
		//Fin computo
		timeEnd = System.currentTimeMillis();
		totalTime = (timeEnd - timeStart) / 1000d;
		
		System.out.println(System.lineSeparator() +
				"The task has taken " + totalTime + " seconds (" +
				ConstructiveAlgorithm.getIterations() + " iterations)");
	}
	
	//This function obtains the path where the instances are stored
	private static Path getPath(String[] args) 
	{
		Path path;		
		if(args.length == 0)
		{
			String separator = System.getProperty("file.separator");
	        //path = Paths.get(".." + separator + "Instancias" + separator + "GKD-Ia" + separator);
	        path = Paths.get("Instancias_MMDP_GKD-Ic_1_20" + separator);
		}
		else
		{
			path = Paths.get(args[0]);
			if(args.length > 1)
			{
				Main.constructiveAlgorithm = Main.setConstructiveAlgorithm(args[1]);
				if(args.length > 2)
				{
					Main.localSearchAlgorithm = Main.setLocalSearchAlgorithm(args[2]);
					if(args.length > 3)
					{
						Main.metaheuristicAlgorithm = Main.setMetaheuristicAlgorithm(args[3]);
						if(args.length > 4)
						{
							Main.writeFile = args[4].equalsIgnoreCase("true") ? true : false;
						}
					}
				}
			}
		}
		return path;
	}

	//Begin compute of the solution
	private static void init(File file) 
	{
		if(file.isDirectory())
		{
			for(File f : file.listFiles())
			{
				Main.compute(f);
				break;
			}
		}
		else
		{
			Main.compute(file);
		}		
	}

	private static void compute(File f)
	{
		final long time = System.currentTimeMillis();
		System.out.println(Main.SEPARATOR + System.lineSeparator());
		String fileStr = f.getAbsolutePath();
		FileRead fr = new FileRead(fileStr);
		ConstructiveAlgorithm[] cas = new ConstructiveAlgorithm[ConstructiveAlgorithm.getIterations()];
		for(int i = 0; i < ConstructiveAlgorithm.getIterations(); i++)
		{
			cas[i] = new ConstructiveAlgorithm(fr);
			cas[i].compute(Main.constructiveAlgorithm);
		}
		
		ConstructiveAlgorithm caBestSolution = Main.getBestSolution(cas);
		System.out.println("Mejor solución (algoritmo de construcción): " + System.lineSeparator() +
				caBestSolution.toString() + System.lineSeparator());
		
		LocalSearchAlgorithm lsa = new LocalSearchAlgorithm(caBestSolution);
		lsa.compute(Main.localSearchAlgorithm);
		System.out.println("Mejor solución (algoritmo de búsqueda): " + System.lineSeparator() +
				lsa.toString() + System.lineSeparator());
		
		MetaheuristicAlgorithm ma = new MetaheuristicAlgorithm(caBestSolution, lsa);
		ma.compute(Main.metaheuristicAlgorithm);
		System.out.println("Mejor solución (metaheurística): " + System.lineSeparator() +
				ma.toString() + System.lineSeparator());
		
		if(Main.writeFile)
		{
			FileWrite.writeFile(Main.constructiveAlgorithm.toString() + " + " +
								Main.localSearchAlgorithm.toString() + " + " + 
								Main.metaheuristicAlgorithm.toString(), 
								caBestSolution.toString() + System.lineSeparator() + 
								lsa.toString() + System.lineSeparator() +
								ma.toString(), (System.currentTimeMillis() - time) / 1000D);
		}
		
		System.out.println(Main.SEPARATOR);
		
	}

	private static Algorithm setConstructiveAlgorithm(String algorithm)
	{
		Algorithm a;
		if(algorithm.equalsIgnoreCase(Algorithm.VORACIOUS.name()))
		{
			a = Algorithm.VORACIOUS;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.VORACIOUS_MULTIBOOT.name()))
		{
			a = Algorithm.VORACIOUS_MULTIBOOT;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.DESTRUCTIVE_VORACIOUS.name()))
		{
			a = Algorithm.DESTRUCTIVE_VORACIOUS;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.DESTRUCTIVE_VORACIOUS_MULTIBOOT.name()))
		{
			a = Algorithm.DESTRUCTIVE_VORACIOUS_MULTIBOOT;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.DESCOMPOSITION.name()))
		{
			a = Algorithm.DESCOMPOSITION;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.REDUCTION.name()))
		{
			a = Algorithm.REDUCTION;
		}
		else
		{
			a = Algorithm.RANDOM;
		}
		return a;
	}
	
	private static Algorithm setMetaheuristicAlgorithm(String algorithm) 
	{
		Algorithm a;
		if(algorithm.equalsIgnoreCase(Algorithm.ITERATED_GREEDY.name()))
		{
			a = Algorithm.ITERATED_GREEDY;
		}
		else
		{
			a = Algorithm.ITERATED_GREEDY;
		}
		return a;
	}
	
	private static Algorithm setLocalSearchAlgorithm(String algorithm)
	{
		Algorithm a;
		if(algorithm.equalsIgnoreCase(Algorithm.BEST_IMPROVEMENT.name()))
		{
			a = Algorithm.BEST_IMPROVEMENT;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.FIRST_LEXICOGRAPHICAL_IMPROVEMENT.name()))
		{
			a = Algorithm.FIRST_LEXICOGRAPHICAL_IMPROVEMENT;
		}
		else
		{
			a = Algorithm.FIRST_RANDOM_IMPROVEMENT;
		}
		return a;
	}

	private static ConstructiveAlgorithm getBestSolution(ConstructiveAlgorithm[] cas) 
	{
		ConstructiveAlgorithm ca = cas[0];
		for(int i = 0; i < cas.length; i++)
		{
			ca = ca.getSolution().minValue().compareTo(cas[i].getSolution().minValue()) < 0 ? cas[i] : ca;
		}
		return ca;
	}

}
