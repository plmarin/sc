package reading;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class FileRead
{
	private int vertex;
	private int edges;
	private boolean[][] adjacentMatrix;
	private List<List<Integer>> adjacentList;
	private File file;
	private String fileName;
	private String abosultePathFileName;
	
	
	public FileRead(String absolutePath)
	{
		this.fileName = absolutePath.substring(absolutePath.lastIndexOf(File.separator) + 1);
		this.abosultePathFileName = absolutePath;
		this.file = new File(this.abosultePathFileName);
		FileReader fr = null;
		BufferedReader br = null;
		int pos_x;
		int pos_y;
		
		try 
		{
			fr = new FileReader(this.file);
			br = new BufferedReader(fr);

			// File reading
			String line = br.readLine();
			String[] elements = line.split(" ");
			// Vertex
			this.vertex = Integer.parseInt(elements[0]);
			// Edges
			this.edges = Integer.parseInt(elements[2]);
			// Store information
			this.adjacentMatrix = new boolean[this.vertex][this.vertex];
			this.adjacentList = new ArrayList<List<Integer>>(this.vertex);
			for(int i = 0; i < this.vertex; i++)
			{
				this.adjacentList.add(new ArrayList<Integer>());
			}
			
			while((line = br.readLine()) != null)
			{
				elements = line.split(" ");
				// Vertex start in 1 (not in 0) --> Avoid ArrayOutOfBounds 
				pos_x = Integer.parseInt(elements[0]) - 1;
				pos_y = Integer.parseInt(elements[1]) - 1;
				this.adjacentMatrix[pos_x][pos_y] = true;
				this.adjacentMatrix[pos_y][pos_x] = true;
				this.adjacentList.get(pos_x).add(pos_y);
				this.adjacentList.get(pos_y).add(pos_x);
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			this.closeFile(fr);
		}
	}
	
	private void closeFile(FileReader fr)
	{
		try 
		{
			if(fr != null) 
			{
				fr.close();
			}
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
	}
	
	public void printAdjacentMatrix()
	{
		for(int i = 0; i < this.adjacentMatrix.length; i++)
		{
			for(int j = 0; j < this.adjacentMatrix[i].length; j++)
			{
				System.out.print(this.adjacentMatrix[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	public void printPositionsAdjacentMatrix()
	{
		for(int i = 0; i < this.adjacentMatrix.length; i++)
			for(int j = i + 1; j < this.adjacentMatrix[i].length; j++)
				System.out.println("AdjacentMatrix[" + (i + 1) + "][" + (j + 1) + "] = " + this.adjacentMatrix[i][j]);
	}
	
	public void printPositionsAdjacentMatrix(boolean condition)
	{
		for(int i = 0; i < this.adjacentMatrix.length; i++)
			for(int j = i + 1; j < this.adjacentMatrix[i].length; j++)
				if(this.adjacentMatrix[i][j] == condition)
					System.out.println("AdjacentMatrix[" + (i + 1) + "][" + (j + 1) + "] = " + this.adjacentMatrix[i][j]);
	}
	
	public void printAdjacentList()
	{
		for(int i = 0; i < this.adjacentList.size(); i++)
		{
			System.out.print("Vertex " + (i + 1) + " adjacent: ");
			for(int j = 0; j < this.adjacentList.get(i).size(); j++)
			{
				System.out.print((this.adjacentList.get(i).get(j) + 1) + " ");
			}
			System.out.println();
		}
	}
	
	public void printReducedAdjacentList()
	{
		int value;
		for(int i = 0; i < this.adjacentList.size(); i++)
		{
			System.out.print("Vertex " + (i + 1) + " adjacent: ");
			for(int j = 0; j < this.adjacentList.get(i).size(); j++)
			{
				value = this.adjacentList.get(i).get(j) + 1;
				if(value > i + 1)
					System.out.print((this.adjacentList.get(i).get(j) + 1) + " ");
			}
			System.out.println();
		}
	}
	
	public boolean checkEdges()
	{
		int matrixEdges = 0;
		for(int i = 0; i < this.adjacentMatrix.length; i++)
			for(int j = i + 1; j < this.adjacentMatrix[i].length; j++)
				if(this.adjacentMatrix[i][j])
					matrixEdges++;
		int listEdges = 0;
		for(List<Integer> list : this.adjacentList)
		{
			listEdges += list.size();
		}
		return matrixEdges == listEdges / 2 ? matrixEdges == this.edges : false;
	}

	public int getVertex() 
	{
		return this.vertex;
	}

	public void setVertex(int vertex) 
	{
		this.vertex = vertex;
	}

	public int getEdges() 
	{
		return this.edges;
	}

	public void setEdges(int edges) 
	{
		this.edges = edges;
	}

	public boolean[][] getAdjacentMatrix() 
	{
		return this.adjacentMatrix;
	}

	public void setAdjacentMatrix(boolean[][] adjacentMatrix)
	{
		this.adjacentMatrix = adjacentMatrix;
	}

	public List<List<Integer>> getAdjacentList() 
	{
		return this.adjacentList;
	}

	public void setAdjacentList(List<List<Integer>> adjacentList) 
	{
		this.adjacentList = adjacentList;
	}

	public File getFile()
	{
		return this.file;
	}

	public String getFileName()
	{
		return this.fileName;
	}
	
	public String getAbosultePathFileName()
	{
		return this.abosultePathFileName;
	}

	public boolean[][] getCopyOfAdjacentMatrix()
	{
		boolean[][] copyOfAdjacentMatrix = new boolean[this.vertex][this.vertex];
		for(int i = 0; i < this.adjacentMatrix.length; i++)
		{
			System.arraycopy(this.adjacentMatrix[i], 0, copyOfAdjacentMatrix[i], 0, this.vertex);
		}
		return copyOfAdjacentMatrix;
	}
	
	public List<List<Integer>> getCopyOfAdjacentList()
	{
		List<List<Integer>> copyOfAdjacentList = new ArrayList<List<Integer>>(this.adjacentList.size());
		for(int i = 0; i < this.adjacentList.size(); i++)
		{
			copyOfAdjacentList.add(new ArrayList<Integer>(this.adjacentList.get(i)));
		}
		return copyOfAdjacentList;
	}
	
}
