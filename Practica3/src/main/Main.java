package main;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import reading.FileRead;
import writing.FileWrite;

import compute.Algorithm;
import compute.ConstructiveAlgorithm;
import compute.LocalSearchAlgorithm;

public class Main
{
	private static Algorithm constructiveAlgorithm = Algorithm.DESTRUCTIVE_VORACIOUS;
	private static Algorithm localSearchAlgorithm = Algorithm.FIRST_RANDOM_IMPROVEMENT;
	private static boolean writeFile = false;
	private static final String SEPARATOR = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"; 
	
	
	public static void main(String[] args) 
	{
		Path path = Main.getPath(args);
		
		System.err.println("PRÁCTICA 3");
		System.err.println("Algoritmos utilizados: " +
				Main.constructiveAlgorithm + " + " + Main.localSearchAlgorithm + System.lineSeparator());
		
		File file = path.toFile();
		if(! file.exists())
		{
			//Si no existe la ruta especificada se le asigna la instancia de las prácticas por defecto
			String separator = System.getProperty("file.separator");
			String defaultPath = "Instancias" + separator + "GKD-Ia" + separator; 
			file = new File(defaultPath);
			
			//Si sigue sin existir se sale del programa con la notificación del error
			if(! file.exists())
			{
				System.err.println("No se pudo acceder a la ruta: " + System.lineSeparator() +
						" \"" + path.toAbsolutePath().normalize().toString() + "\"." + System.lineSeparator() +
						"Tampoco se pudo acceder a la ruta por defecto: " + System.lineSeparator() +
						" \"" + Paths.get(defaultPath).toAbsolutePath().toString() + "\".");
				System.exit(-1);
			}
		}
		
		//Inicio cómputo
		final long timeStart;
		final long timeEnd;
		final double totalTime;
		timeStart = System.currentTimeMillis();
		
		Main.init(file);
		
		//Fin cómputo
		timeEnd = System.currentTimeMillis();
		totalTime = (timeEnd - timeStart) / 1000d;
		
		System.err.println(System.lineSeparator() +
				"The task has taken " + totalTime + " seconds (" +
				ConstructiveAlgorithm.getIterations() + " iterations)");
	}

	private static Path getPath(String[] args) 
	{
		Path path;		
		if(args.length == 0)
		{
			String separator = System.getProperty("file.separator");
	        path = Paths.get(".." + separator + "Instancias" + separator + "GKD-Ia" + separator);
	        //path = Paths.get(".." + separator + "Instancias" + separator + "04_MMDP_GKD-Ic_1_10" + separator);
		}
		else
		{
			path = Paths.get(args[0]);
			if(args.length > 1)
			{
				Main.constructiveAlgorithm = Main.setConstructiveAlgorithm(args[1]);
				if(args.length > 2)
				{
					Main.localSearchAlgorithm = Main.setLocalSearchAlgorithm(args[2]);
					if(args.length > 3)
					{
						Main.writeFile = args[3].equalsIgnoreCase("true") ? true : false;
					}
				}
			}
		}
		return path;
	}

	private static void init(File file) 
	{
		if(file.isDirectory())
		{
			for(File f : file.listFiles())
			{
				Main.compute(f);
				break;
			}
		}
		else
		{
			Main.compute(file);
		}		
	}

	private static void compute(File f)
	{
		System.out.println(Main.SEPARATOR + System.lineSeparator());
		String fileStr = f.getAbsolutePath();
		FileRead fr = new FileRead(fileStr);
		ConstructiveAlgorithm[] cas = new ConstructiveAlgorithm[ConstructiveAlgorithm.getIterations()];
		for(int i = 0; i < ConstructiveAlgorithm.getIterations(); i++)
		{
			cas[i] = new ConstructiveAlgorithm(fr);
			cas[i].compute(Main.constructiveAlgorithm);
		}
		
		ConstructiveAlgorithm bestSolution = Main.getBestSolution(cas);
		LocalSearchAlgorithm lsa = new LocalSearchAlgorithm(bestSolution);
		lsa.compute(Main.localSearchAlgorithm);
		if(Main.writeFile)
		{
			FileWrite.writeFile(Main.constructiveAlgorithm.toString() + " + " +
								Main.localSearchAlgorithm.toString(), bestSolution.toString() + 
								System.lineSeparator() + lsa.toString());
		}
		
		System.out.println("Mejor solución (algoritmo de construcción): " + System.lineSeparator() +
								bestSolution.toString() + System.lineSeparator());
		System.out.println("Mejor solución (algoritmo de búsqueda): " + System.lineSeparator() +
								lsa.toString() + System.lineSeparator());
		System.out.println(Main.SEPARATOR);
	}

	private static Algorithm setConstructiveAlgorithm(String algorithm)
	{
		Algorithm a;
		if(algorithm.equalsIgnoreCase(Algorithm.VORACIOUS.name()))
		{
			a = Algorithm.VORACIOUS;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.VORACIOUS_MULTIBOOT.name()))
		{
			a = Algorithm.VORACIOUS_MULTIBOOT;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.DESTRUCTIVE_VORACIOUS.name()))
		{
			a = Algorithm.DESTRUCTIVE_VORACIOUS;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.DESTRUCTIVE_VORACIOUS_MULTIBOOT.name()))
		{
			a = Algorithm.DESTRUCTIVE_VORACIOUS_MULTIBOOT;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.DESCOMPOSITION.name()))
		{
			a = Algorithm.DESCOMPOSITION;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.REDUCTION.name()))
		{
			a = Algorithm.REDUCTION;
		}
		else
		{
			a = Algorithm.RANDOM;
		}
		return a;
	}
	
	private static Algorithm setLocalSearchAlgorithm(String algorithm)
	{
		Algorithm a;
		if(algorithm.equalsIgnoreCase(Algorithm.BEST_IMPROVEMENT.name()))
		{
			a = Algorithm.BEST_IMPROVEMENT;
		}
		else if(algorithm.equalsIgnoreCase(Algorithm.FIRST_LEXICOGRAPHICAL_IMPROVEMENT.name()))
		{
			a = Algorithm.FIRST_LEXICOGRAPHICAL_IMPROVEMENT;
		}
		else
		{
			a = Algorithm.FIRST_RANDOM_IMPROVEMENT;
		}
		return a;
	}

	private static ConstructiveAlgorithm getBestSolution(ConstructiveAlgorithm[] cas) 
	{
		ConstructiveAlgorithm ca = cas[0];
		for(int i = 0; i < cas.length; i++)
		{
			ca = ca.getSolution().minValue().compareTo(cas[i].getSolution().minValue()) < 0 ? cas[i] : ca;
		}
		return ca;
	}

}
