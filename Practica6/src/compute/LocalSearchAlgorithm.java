package compute;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import reading.FileRead;
import solution.Solution;

/*
 * This class compute a solution from giving solution
 * through some local search heuristics (stipulated in the Algorithm class)
 */
public class LocalSearchAlgorithm implements ComputeAlgorithm 
{
	private final FileRead FILE_READ;
	private Solution solution;

	
	public LocalSearchAlgorithm(ConstructiveAlgorithm constructiveAlgorithm) 
	{
		this(constructiveAlgorithm.getFileRead(),
			constructiveAlgorithm.getSolution());
	}
	
	public LocalSearchAlgorithm(FileRead fileRead, Solution solution) 
	{
		this.FILE_READ = fileRead;
		this.solution = new Solution(solution);
	}
	
	public LocalSearchAlgorithm(FileRead fileRead) 
	{
		this.FILE_READ = fileRead;
		this.solution = null;
	}
	
	// Compute Algorithm
	@Override
	public void compute(Algorithm algorithm)
	{
		if(algorithm.name().equals(Algorithm.BEST_IMPROVEMENT.name()))
		{
			this.bestImprovement();
		}
		else if(algorithm.name().equalsIgnoreCase(Algorithm.FIRST_LEXICOGRAPHICAL_IMPROVEMENT.name()))
		{
			this.firstLexicographicalImprovement();
		}
		else
		{
			this.firstRandomImprovement();
		}
	}
	
	/*
	 * FIRST RANDOM IMPROVEMENT
	 */
	private void firstRandomImprovement() 
	{
		Solution bestSolution = new Solution(this.solution);
		boolean improve;
		int[] hubsIndex = bestSolution.getHubsIndex();
		Random rnd = new Random(hubsIndex.length);
		List<Integer> nodeList;
		List<Integer> auxList;

		int indexNetList;
		for(int hubIndex = 0; hubIndex < hubsIndex.length; hubIndex++)
		{
			nodeList = new ArrayList<Integer>(bestSolution.getHubNetList()[hubIndex].size() - 1);
			for(int i = 1; i < bestSolution.getHubNetList()[hubIndex].size(); i++)
			{
				nodeList.add(i);
			}
			auxList = new ArrayList<Integer>(nodeList);
			
			// Iterate until not find improvement
			do
			{
				improve = false;
				for(int i = 1; i < bestSolution.getHubNetList()[hubIndex].size(); i++)
				{
					indexNetList = nodeList.remove(rnd.nextInt(nodeList.size()));
					bestSolution.changeHub(hubIndex, indexNetList);
					
					if(this.solution.compareTo(bestSolution) == 1)
					{
						this.solution = new Solution(bestSolution);
						improve = true;
						nodeList = new ArrayList<Integer>(auxList);
						break;
					}
					if(improve)
					{
						break;
					}
				}
			}while(improve);
			bestSolution = new Solution(this.solution);
		}
	}

	/*
	 * FIRST LEXICOGRAPHICAL IMPROVEMENT
	 */
	private void firstLexicographicalImprovement() 
	{
		Solution bestSolution = new Solution(this.solution);
		boolean improve;
		int[] hubsIndex = bestSolution.getHubsIndex();

		for(int hubIndex = 0; hubIndex < hubsIndex.length; hubIndex++)
		{
			// Iterate until not find improvement
			do
			{
				improve = false;
				for(int indexNetList = 1; indexNetList < bestSolution.getHubNetList()[hubIndex].size(); indexNetList++)
				{
					bestSolution.changeHub(hubIndex, indexNetList);
					
					if(this.solution.compareTo(bestSolution) == 1)
					{
						this.solution = new Solution(bestSolution);
						improve = true;
						break;
					}
					if(improve)
					{
						break;
					}
				}
			}while(improve);
			bestSolution = new Solution(this.solution);
		}
	}

	/*
	 * BEST IMPROVEMENT
	 */
	private void bestImprovement() 
	{
		System.err.println(Algorithm.BEST_IMPROVEMENT.name() + " algorithm hasn't implement yet." +
				" Try with other local search algorithm");
		System.exit(-1);
	}

	public FileRead getFileRead() 
	{
		return this.FILE_READ;
	}

	@Override
	public Solution getSolution() 
	{
		return this.solution;
	}
	
	public void setSolution(Solution solution) 
	{
		this.solution = solution;
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[absolutePathFileRead= \"" + this.FILE_READ.getAbosultePathFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("fileRead= \"" + this.FILE_READ.getFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("solution= " + this.solution);
		sb.append("," + System.lineSeparator());	
		sb.append("Objetive Function= " + this.solution.getDistance() + "]");
		return sb.toString();
	}

}
