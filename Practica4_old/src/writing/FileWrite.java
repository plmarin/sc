package writing;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileWrite
{
	private static final String fileName = "Practica4_old.txt";
	
	public static void writeFile(String algorithm, String bestSolution, double seconds) 
	{
        String separator = System.getProperty("file.separator");
        Path path = Paths.get(".." + separator + FileWrite.fileName);
        FileWriter file = null;
        PrintWriter pw = null;
        try
        {
        	file = new FileWriter(path.toAbsolutePath().toString(), true);
            pw = new PrintWriter(file);
 
            pw.println(algorithm + " algorithm");
            pw.println(bestSolution + System.lineSeparator());     
            pw.println("The task has taken " + seconds + " seconds" + System.lineSeparator());
            System.out.println("Fichero escrito en: " + path.toAbsolutePath().toString());
        } 
        catch(Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
           try 
           {
	           if(null != file)
	              pw.close();
           } 
           catch(Exception e) 
           {
              e.printStackTrace();
           }
        }
	}
	
}
