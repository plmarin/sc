package compute;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import reading.FileRead;
import bean.Point;
import bean.Solution;

/*
 * This class compute a solution through some constructive heuristics (stipulated in the Algorithm class)
 */
public class ComputeSolution
{	
	private final FileRead fileRead;
	private Set<Integer> solutionPoints;
	private Solution solution;
	private static int iterations;
	private static long[] rand;
	private static int randCount;
	
	
	static
	{
		ComputeSolution.iterations = 1;
		ComputeSolution.randCount = 0;
		ComputeSolution.rand = new long[ComputeSolution.iterations];
		for(int i = 0; i < ComputeSolution.iterations; i++)
		{
			ComputeSolution.rand[i] = i;
		}
	}
	
	public ComputeSolution(FileRead fileRead)
	{
		if(ComputeSolution.randCount >= ComputeSolution.iterations)
			ComputeSolution.randCount = 0;
		this.fileRead = fileRead;
		this.solutionPoints = new HashSet<Integer>(this.fileRead.getChoosen());
		this.solution = new Solution();
	}
	
	/*
	 * RANDOM HEURISTIC
	 */
	private void random()
	{
		int x;
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		Random rnd = new Random(ComputeSolution.rand[ComputeSolution.randCount++]);
		for(int i = 0; i < this.fileRead.getChoosen(); i++)
		{
			do
			{
				x = rnd.nextInt(matrixDistances.length);
			} while(ComputeSolution.containsPoint(this.solutionPoints, x));
			//Se cogen todos los datos del punto seleccionado
			this.fillSolution(x, matrixDistances[x]);
			this.solutionPoints.add(x);
		}		
	}
	
	/*
	 * VORACIOUS HEURISTIC - Multiboot
	 */
	private void voraciousMultiboot()
	{
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		float[][] copyOfMatrixDistances = this.fileRead.getCopyOfMatrixDistances();
		Random rnd = new Random(ComputeSolution.rand[ComputeSolution.randCount++]);
		int x = rnd.nextInt(matrixDistances.length);
		int new_x;
		do
		{
			this.fillSolution(x, matrixDistances[x]);
			this.solutionPoints.add(x);
			new_x = this.searchBestValue(copyOfMatrixDistances[x]);
			this.cleanPosition(copyOfMatrixDistances, x, new_x, Float.NEGATIVE_INFINITY);
			x = new_x;
		}while(this.solutionPoints.size() < this.fileRead.getChoosen());
	}
	
	/*
	 * VORACIOUS HEURISTIC
	 */
	private void voracious()
	{
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		float[][] copyOfMatrixDistances = this.fileRead.getCopyOfMatrixDistances();
		int x;
		do
		{
			x = this.searchBestValue(copyOfMatrixDistances, true);
			this.fillSolution(x, matrixDistances[x]);
			this.solutionPoints.add(x);
		}while(this.solutionPoints.size() < this.fileRead.getChoosen());
	}
	
	/*
	 * DESTRUCTIVE_VORACIOUS HEURISTIC - Multiboot
	 */
	private void destructiveVoraciousMultiboot() 
	{
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		float[][] copyOfMatrixDistances = this.fileRead.getCopyOfMatrixDistances();
		Random rnd = new Random(ComputeSolution.rand[ComputeSolution.randCount++]);
		int x = rnd.nextInt(matrixDistances.length);
		int new_x;
		Set<Integer> solutionPoints = new HashSet<Integer>(this.fileRead.getSize());
		for(int i = 0; i < this.fileRead.getSize(); i++)
		{
			solutionPoints.add(i);
			this.cleanPosition(copyOfMatrixDistances, i, i, Float.POSITIVE_INFINITY);
		}	
		do
		{
			solutionPoints.remove(x);
			new_x = this.searchWorstValue(copyOfMatrixDistances[x]);
			this.cleanPosition(copyOfMatrixDistances, x, new_x, Float.POSITIVE_INFINITY);
			x = new_x;
		}while(solutionPoints.size() > this.fileRead.getChoosen());
		for(Integer i : solutionPoints)
		{
			this.fillSolution(i, matrixDistances[i]);
			this.solutionPoints.add(i);
		}
	}
	
	/*
	 * DESTRUCTIVE_VORACIOUS HEURISTIC
	 */
	private void destructiveVoracious() 
	{
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		float[][] copyOfMatrixDistances = this.fileRead.getCopyOfMatrixDistances();
		int x;
		Set<Integer> solutionPoints = new HashSet<Integer>(this.fileRead.getSize());
		for(int i = 0; i < this.fileRead.getSize(); i++)
		{
			solutionPoints.add(i);
			this.cleanPosition(copyOfMatrixDistances, i, i, Float.POSITIVE_INFINITY);
		}	
		do
		{
			x = this.searchWorstValue(copyOfMatrixDistances, true);
			solutionPoints.remove(x);
		}while(solutionPoints.size() > this.fileRead.getChoosen());
		for(Integer i : solutionPoints)
		{
			this.fillSolution(i, matrixDistances[i]);
			this.solutionPoints.add(i);
		}
	}
	
	/*
	 * DESCOMPOSITION
	 */
	private void descomposition()
	{
		//Como divide y vencerás
	}
	
	/*
	 * REDUCTION
	 */
	private void reduction()
	{
		//Mirar apuntes
	}

	//Compute Algorithm
	public void compute(Algorithm algorithm)
	{
		if(algorithm.name().equals(Algorithm.VORACIOUS.name()))
		{
			this.voracious();
		}
		else if(algorithm.name().equals(Algorithm.VORACIOUS_MULTIBOOT.name()))
		{
			this.voraciousMultiboot();
		}
		else if(algorithm.name().equals(Algorithm.DESTRUCTIVE_VORACIOUS.name()))
		{
			this.destructiveVoracious();
		}
		else if(algorithm.name().equals(Algorithm.DESTRUCTIVE_VORACIOUS_MULTIBOOT.name()))
		{
			this.destructiveVoraciousMultiboot();
		}
		else if(algorithm.name().equals(Algorithm.DESCOMPOSITION.name()))
		{
			this.descomposition();
		}
		else if(algorithm.name().equals(Algorithm.REDUCTION.name()))
		{
			this.reduction();
		}
		else
		{
			this.random();
		}
	}

	private void cleanPosition(float[][] copyOfMatrixDistances, int x, int new_x, float value) 
	{
		copyOfMatrixDistances[x][new_x] = value;
		copyOfMatrixDistances[new_x][x] = value;
	}
	
	private void cleanRow(float[] fs, float value) 
	{
		for(int i = 0; i < fs.length; i++)
		{
			fs[i] = value;
		}
	}
	
	private int searchWorstValue(float[] fs) 
	{
		int posWorstValue = 0;
		float worstValue = Float.POSITIVE_INFINITY;
		for(int i = 0; i < fs.length; i++)
		{			
				if(! this.solutionPoints.contains(i) && worstValue > fs[i])
				{
					worstValue = fs[i];
					posWorstValue = i;
				}
		}
		return posWorstValue;
	}
	
	private int searchBestValue(float[] fs) 
	{
		int posBestValue = 0;
		float bestValue = Float.NEGATIVE_INFINITY;
		for(int i = 0; i < fs.length; i++)
		{			
				if(! this.solutionPoints.contains(i) && bestValue < fs[i])
				{
					bestValue = fs[i];
					posBestValue = i;
				}
		}
		return posBestValue;
	}
	
	private int searchWorstValue(float[][] matrixDistances, boolean cleanPosition) 
	{
		List<Integer> worstPositionsValueX = new ArrayList<Integer>();
		List<Integer> worstPositionsValueY = new ArrayList<Integer>();
		float worstValue = Float.POSITIVE_INFINITY;
		for(int i = 0; i < matrixDistances.length; i++)
		{
			for(int j = 0; j < i; j++)
			{
				if(worstValue > matrixDistances[i][j])
				{
					worstValue = matrixDistances[i][j];
					worstPositionsValueX.clear();
					worstPositionsValueY.clear();
					worstPositionsValueX.add(i);
					worstPositionsValueY.add(j);
				}
				else if(worstValue == matrixDistances[i][j])
				{
					worstPositionsValueX.add(i);
					worstPositionsValueY.add(j);
				}
			}
		}
		Random rnd = new Random(ComputeSolution.randCount);
		int index = rnd.nextInt(worstPositionsValueX.size());
		int worstPositionValueX = worstPositionsValueX.get(index);
		if(cleanPosition)
		{
			int worstPositionValueY = worstPositionsValueY.get(index);
			this.cleanPosition(matrixDistances, worstPositionValueX, worstPositionValueY, Float.POSITIVE_INFINITY);
			this.cleanRow(matrixDistances[worstPositionValueX], Float.POSITIVE_INFINITY);
		}
		return worstPositionValueX;
	}
		
	private int searchBestValue(float[][] matrixDistances, boolean cleanPosition) 
	{
		List<Integer> bestPositionsValueX = new ArrayList<Integer>();
		List<Integer> bestPositionsValueY = new ArrayList<Integer>();
		float bestValue = Float.NEGATIVE_INFINITY;
		for(int i = 0; i < matrixDistances.length; i++)
		{
			for(int j = 0; j < i; j++)
			{
				if(bestValue < matrixDistances[i][j])
				{
					bestValue = matrixDistances[i][j];
					bestPositionsValueX.clear();
					bestPositionsValueY.clear();
					bestPositionsValueX.add(i);
					bestPositionsValueY.add(j);
				}
				else if(bestValue == matrixDistances[i][j])
				{
					bestPositionsValueX.add(i);
					bestPositionsValueY.add(j);
				}
			}
		}
		Random rnd = new Random(ComputeSolution.randCount);
		int index = rnd.nextInt(bestPositionsValueX.size());
		int bestPositionValueX = bestPositionsValueX.get(index);
		if(cleanPosition)
		{
			int bestPositionValueY = bestPositionsValueY.get(index);
			this.cleanPosition(matrixDistances, bestPositionValueX, bestPositionValueY, Float.NEGATIVE_INFINITY);
			this.cleanRow(matrixDistances[bestPositionValueX], Float.NEGATIVE_INFINITY);
		}
		return bestPositionValueX;
	}
	
	private static boolean containsPoint(Set<Integer> solutionPoints, int p) 
	{
		for(int i : solutionPoints)
		{
			if(i == p)
			{
				return true;
			}
		}
		return false;
	}

	private void fillSolution(int pos_x, float[] vectorDistances)
	{
		List<Point> points = this.solution.getPoints();
		for(int i : this.solutionPoints)
		{
			points.add(new Point(pos_x, i, vectorDistances[i]));
		}
	}

	public FileRead getFileRead() {
		return fileRead;
	}

	public Set<Integer> getSolutionPoints() {
		return solutionPoints;
	}

	public Solution getSolution() {
		return solution;
	}

	public static int getIterations() {
		return iterations;
	}

	public static void setIterations(int iterations) {
		ComputeSolution.iterations = iterations;
	}

	public static long[] getRand() {
		return rand;
	}

	public static void setRand(long[] rand) {
		ComputeSolution.rand = rand;
	}

	public static int getRandCount() {
		return randCount;
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[fileRead="); 
		sb.append(this.fileRead.getFileName() + ", " + System.lineSeparator() + "solutionPoints=[");
		for(int i : this.solutionPoints)
		{
			sb.append(i + ", ");
		}
		//sb.insert(sb.capacity() - 2, "]");
		sb.append("]," + System.lineSeparator() + "solution=" + this.solution);
		sb.append("," + System.lineSeparator() + "Objetive Function= " + this.solution.minValue() + "]");
		return sb.toString();
	}
	
}
