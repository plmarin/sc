package compute;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import reading.FileRead;
import solution.Solution;
import utils.Utils;

/*
 * This class compute a solution through some constructive heuristics (stipulated in the Algorithm class)
 */
public class ConstructiveAlgorithm implements ComputeAlgorithm
{	
	private final FileRead FILE_READ;
	private Solution solution;
	private static final int ITERATIONS;
	private static final long[] RAND;
	private static int randCount;
	
	
	static
	{
		ITERATIONS = 1;
		RAND = new long[ConstructiveAlgorithm.ITERATIONS];
		for(int i = 0; i < ConstructiveAlgorithm.ITERATIONS; i++)
		{
			ConstructiveAlgorithm.RAND[i] = i;
		}
		ConstructiveAlgorithm.randCount = 0;
	}
	
	public ConstructiveAlgorithm(FileRead fileRead)
	{
		this(fileRead, new Solution(fileRead.getChoosen()));
	}
	
	public ConstructiveAlgorithm(FileRead fileRead, Solution solution)
	{
		if(ConstructiveAlgorithm.randCount >= ConstructiveAlgorithm.ITERATIONS)
			ConstructiveAlgorithm.randCount = 0;
		this.FILE_READ = fileRead;
		Solution.setMatrixDistances(this.FILE_READ.getMatrixDistances());
		this.solution = solution;
	}
	
	// Compute Algorithm
	@Override
	public void compute(Algorithm algorithm)
	{
		if(algorithm.name().equals(Algorithm.VORACIOUS.name()))
		{
			this.voracious();
		}
		else if(algorithm.name().equals(Algorithm.VORACIOUS_MULTIBOOT.name()))
		{
			this.voraciousMultiboot();
		}
		else if(algorithm.name().equals(Algorithm.DESTRUCTIVE_VORACIOUS.name()))
		{
			this.destructiveVoracious();
		}
		else if(algorithm.name().equals(Algorithm.DESTRUCTIVE_VORACIOUS_MULTIBOOT.name()))
		{
			this.destructiveVoraciousMultiboot();
		}
		else if(algorithm.name().equals(Algorithm.DESCOMPOSITION.name()))
		{
			this.descomposition();
		}
		else if(algorithm.name().equals(Algorithm.REDUCTION.name()))
		{
			this.reduction();
		}
		else
		{
			this.random();
		}
	}
	
	/*
	 * RANDOM HEURISTIC
	 */
	private void random()
	{
		int x;
		float[][] matrixDistances = this.FILE_READ.getMatrixDistances();
		Random rnd = new Random(ConstructiveAlgorithm.RAND[ConstructiveAlgorithm.randCount++]);
		for(int i = this.solution.size(); i < this.FILE_READ.getChoosen(); i++)
		{
			do
			{
				x = rnd.nextInt(matrixDistances.length);
			}while(this.solution.contains(x));
			// Se cogen todos los datos del punto seleccionado
			this.solution.addPoint(x);
		}		
	}
	
	/*
	 * VORACIOUS HEURISTIC - Multiboot
	 */
	// Multiboot --> Cogiendo un elemento distinto en cada iteracion
	private void voraciousMultiboot()
	{
		float[][] matrixDistances = this.FILE_READ.getMatrixDistances();
		Random rnd = new Random(ConstructiveAlgorithm.RAND[ConstructiveAlgorithm.randCount++]);
		int x = rnd.nextInt(matrixDistances.length);
		this.solution.addPoint(x);
		while(this.solution.size() < this.FILE_READ.getChoosen())
		{
			x = this.searchBestValue(matrixDistances[x]);
			this.solution.addPoint(x);
		}
	}
	
	/*
	 * VORACIOUS HEURISTIC
	 */
	private void voracious()
	{
		// Method 1: Faster but worse
		/*
		float[][] matrixDistances = this.fileRead.getMatrixDistances();
		int x = this.searchBestValue(matrixDistances, false);
		this.solution.addPoint(x);
		while(this.solution.size() < this.fileRead.getChoosen())
		{
			x = this.searchBestValue(matrixDistances[x]);
			this.solution.addPoint(x);
		}
		*/
		
		// Method 2: Better but slower
		float[][] copyOfMatrixDistances = this.FILE_READ.getCopyOfMatrixDistances();
		int x;
		do
		{
			x = this.searchBestValue(copyOfMatrixDistances, true);
			this.solution.addPoint(x);
		}while(this.solution.size() < this.FILE_READ.getChoosen());
	}
	
	/*
	 * DESTRUCTIVE_VORACIOUS HEURISTIC - Multiboot
	 */
	// Multiboot --> Cogiendo un elemento distinto en cada iteracion
	private void destructiveVoraciousMultiboot() 
	{
		float[][] copyOfMatrixDistances = this.FILE_READ.getCopyOfMatrixDistances();
		Random rnd = new Random(ConstructiveAlgorithm.RAND[ConstructiveAlgorithm.randCount++]);
		Set<Integer> solutionPoints = new HashSet<Integer>(this.FILE_READ.getSize());
		for(int i = 0; i < this.FILE_READ.getSize(); i++)
		{
			solutionPoints.add(i);
			this.cleanPosition(copyOfMatrixDistances, i, i, Float.POSITIVE_INFINITY);
		}
		
		int x = rnd.nextInt(copyOfMatrixDistances.length);
		int new_x;
		while(solutionPoints.size() > this.FILE_READ.getChoosen())
		{
			solutionPoints.remove(x);
			new_x = this.searchWorstValue(copyOfMatrixDistances[x]);
			this.cleanPosition(copyOfMatrixDistances, x, new_x, Float.POSITIVE_INFINITY);
			x = new_x;
		}
		
		for(Integer i : solutionPoints)
		{
			this.solution.addPoint(i);
		}
	}
	
	/*
	 * DESTRUCTIVE_VORACIOUS HEURISTIC
	 */
	private void destructiveVoracious() 
	{
		float[][] copyOfMatrixDistances = this.FILE_READ.getCopyOfMatrixDistances();
		int x;
		Set<Integer> solutionPoints = this.solution.getPoints();
		
		// Se anaden todos los elementos a la solucion
		for(int i = 0; i < this.FILE_READ.getSize(); i++)
		{
			solutionPoints.add(i);
			// Los puntos (0,0), (1,1)... se establecen ahora como "infinito" en lugar de 0
			// para poder buscar el peor elemento despues (el mas pequeno)
			this.cleanPosition(copyOfMatrixDistances, i, i, Float.POSITIVE_INFINITY);
		}
		
		// Se eliminan aquellos elemenos de la solucion que no son adecuados
		// hasta que el tamano de la solucion se quede en el tamano indicado (this.file.getChoosen())
		while(solutionPoints.size() > this.FILE_READ.getChoosen())
		{
			// Busqueda del elemento mas pequeno para sacarlo de la solucion
			x = this.searchWorstValue(copyOfMatrixDistances, true);
			solutionPoints.remove(x);
		}
	}
	
	/*
	 * DESCOMPOSITION
	 */
	private void descomposition()
	{
		System.err.println(Algorithm.DESCOMPOSITION.name() + " algorithm hasn't implement yet." +
				" Try with other constructive algorithm");
		System.exit(-1);
	}
	
	/*
	 * REDUCTION
	 */
	private void reduction()
	{
		System.err.println(Algorithm.REDUCTION.name() + " algorithm hasn't implement yet." +
				" Try with other constructive algorithm");
		System.exit(-1);
	}

	private void cleanPosition(float[][] copyOfMatrixDistances, int x, int new_x, float value) 
	{
		copyOfMatrixDistances[x][new_x] = value;
		copyOfMatrixDistances[new_x][x] = value;
	}
	
	private void cleanRow(float[] fs, float value) 
	{
		for(int i = 0; i < fs.length; i++)
		{
			fs[i] = value;
		}
	}
	
	private int searchWorstValue(float[] fs) 
	{
		int posWorstValue = 0;
		float worstValue = Float.POSITIVE_INFINITY;
		for(int i = 0; i < fs.length; i++)
		{			
			if(! this.solution.contains(i) && worstValue > fs[i])
			{
				worstValue = fs[i];
				posWorstValue = i;
			}
		}
		return posWorstValue;
	}
	
	private int searchBestValue(float[] fs) 
	{
		int posBestValue = 0;
		float bestValue = Float.NEGATIVE_INFINITY;
		for(int i = 0; i < fs.length; i++)
		{			
			if(! this.solution.contains(i) && bestValue < fs[i])
			{
				bestValue = fs[i];
				posBestValue = i;
			}
		}
		return posBestValue;
	}
	
	private int searchWorstValue(float[][] matrixDistances, boolean cleanPosition) 
	{
		List<Integer> worstPositionsValueX = new ArrayList<Integer>();
		List<Integer> worstPositionsValueY = new ArrayList<Integer>();
		float worstValue = Float.POSITIVE_INFINITY;
		for(int i = 0; i < matrixDistances.length; i++)
		{
			for(int j = 0; j < i; j++)
			{
				if(worstValue > matrixDistances[i][j])
				{
					worstValue = matrixDistances[i][j];
					worstPositionsValueX.clear();
					worstPositionsValueY.clear();
					worstPositionsValueX.add(i);
					worstPositionsValueY.add(j);
				}
				else if(worstValue == matrixDistances[i][j])
				{
					worstPositionsValueX.add(i);
					worstPositionsValueY.add(j);
				}
			}
		}
		Random rnd = new Random(ConstructiveAlgorithm.randCount);
		int index = rnd.nextInt(worstPositionsValueX.size());
		int worstPositionValueX = worstPositionsValueX.get(index);
		if(cleanPosition)
		{
			int worstPositionValueY = worstPositionsValueY.get(index);
			this.cleanPosition(matrixDistances, worstPositionValueX, worstPositionValueY, Float.POSITIVE_INFINITY);
			this.cleanRow(matrixDistances[worstPositionValueX], Float.POSITIVE_INFINITY);
		}
		return worstPositionValueX;
	}
		
	private int searchBestValue(float[][] matrixDistances, boolean cleanPosition) 
	{
		List<Integer> bestPositionsValueX = new ArrayList<Integer>();
		List<Integer> bestPositionsValueY = new ArrayList<Integer>();
		float bestValue = Float.NEGATIVE_INFINITY;
		for(int i = 0; i < matrixDistances.length; i++)
		{
			for(int j = 0; j < i; j++)
			{
				if(bestValue < matrixDistances[i][j])
				{
					bestValue = matrixDistances[i][j];
					bestPositionsValueX.clear();
					bestPositionsValueY.clear();
					bestPositionsValueX.add(i);
					bestPositionsValueY.add(j);
				}
				else if(bestValue == matrixDistances[i][j])
				{
					bestPositionsValueX.add(i);
					bestPositionsValueY.add(j);
				}
			}
		}
		Random rnd = new Random(ConstructiveAlgorithm.randCount++);
		int index = rnd.nextInt(bestPositionsValueX.size());
		int bestPositionValueX = bestPositionsValueX.get(index);
		if(cleanPosition)
		{
			int bestPositionValueY = bestPositionsValueY.get(index);
			this.cleanPosition(matrixDistances, bestPositionValueX, bestPositionValueY, Float.NEGATIVE_INFINITY);
			this.cleanRow(matrixDistances[bestPositionValueX], Float.NEGATIVE_INFINITY);
		}
		return bestPositionValueX;
	}

	public final FileRead getFileRead() 
	{
		return this.FILE_READ;
	}

	@Override
	public Solution getSolution() 
	{
		return this.solution;
	}

	public static int getIterations() 
	{
		return ConstructiveAlgorithm.ITERATIONS;
	}

	public static long[] getRand() 
	{
		return ConstructiveAlgorithm.RAND;
	}

	public static int getRandCount() 
	{
		return ConstructiveAlgorithm.randCount;
	}

	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[absolutePathFileRead= \"" + this.FILE_READ.getAbosultePathFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("fileRead= \"" + this.FILE_READ.getFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("solution= " + this.solution);
		sb.append("," + System.lineSeparator());	
		sb.append("Objetive Function= " + Utils.roundToNDecimals(this.solution.minValue(), 2) + "]");
		return sb.toString();
	}
	
}
