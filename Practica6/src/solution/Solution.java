package solution;

import java.util.ArrayList;
import java.util.List;

import reading.FileRead;
import bean.Node;

public class Solution implements Comparable<Solution>
{
	private static float[][] allDistances;
	private List<Node>[] hubNetList;
	private float[] distances;
	private Node[] nodeArray;
	private int[] hubsIndex;
	private int hubsAdded;
	
	
	@SuppressWarnings("unchecked")
	public Solution(FileRead fileRead)
	{
		Solution.allDistances = fileRead.getAllDistances();
		this.hubNetList = new List[fileRead.getHubs()];
		for(int i = 0; i < this.hubNetList.length; i++)
		{
			this.hubNetList[i] = new ArrayList<Node>();
		}
		this.distances = new float[fileRead.getHubs()];
		for(int i = 0; i < this.distances.length; i++)
		{
			this.distances[i] = 0.0f;
		}
		this.nodeArray = fileRead.getCopyOfNodeArray();
		this.hubsIndex = new int[fileRead.getHubs()];
		this.hubsAdded = 0;
	}
	
	@SuppressWarnings("unchecked")
	public Solution(Solution solution) 
	{
		this.hubNetList = new List[solution.getHubNetList().length];
		for(int i = 0; i < this.hubNetList.length; i++)
		{
			this.hubNetList[i] = new ArrayList<Node>(solution.getHubNetList()[i]);
		}
		this.distances = new float[solution.getDistances().length];
		System.arraycopy(solution.getDistances(), 0, this.distances, 0, this.distances.length);
		this.nodeArray = new Node[solution.getNodeArray().length];
		for(int i = 0; i < this.nodeArray.length; i++)
		{
			this.nodeArray[i] = new Node(solution.nodeArray[i]);
		}
		this.hubsIndex = new int[solution.getHubsIndex().length];
		System.arraycopy(solution.getHubsIndex(), 0, this.hubsIndex, 0, this.hubsIndex.length);
		this.hubsAdded = solution.getHubsAdded();
	}
	
	public void addHubs(int[] hubsIndex) 
	{
		for(int hubIndex : hubsIndex)
		{
			this.addHub(hubIndex);
		}		
	}
	
	public void addHub(int hubIndex)
	{
		this.hubNetList[this.hubsAdded].add(this.nodeArray[hubIndex]);
		this.nodeArray[hubIndex].setHub(true);
		this.hubsIndex[this.hubsAdded++] = hubIndex;
	}
	
	public boolean addNode(int hubIndex, int nodeIndex)
	{
		boolean added = false;
		
		// Hub search by index
		Node hub = this.nodeArray[this.hubsIndex[hubIndex]];
		
		// Node search by index
		Node node = this.nodeArray[nodeIndex];
		
		// Are there enough capacity? -> Add node
		if(hub.getCapacity() >= node.getDemandValue())
		{
			// Update hub capacity
			hub.setCapacity(hub.getCapacity() - node.getDemandValue());
			
			// Now this node is busy
			node.setFree(false);
			added = true;
			
			// Add node to its hub
			this.hubNetList[hubIndex].add(node);
			
			// Compute distance
			//float distance = Utils.euclideanDistance(hub.getPoint(), node.getPoint());
			float distance = Solution.allDistances[this.hubsIndex[hubIndex]][nodeIndex];
			
			// Update distances
			this.distances[hubIndex] += distance;
		}
		return added;
	}
	
	public boolean freeNodes()
	{
		int busyNodes = 0;
		for(List<Node> list : this.hubNetList)
		{
			busyNodes += list.size();
		}
		return busyNodes < this.nodeArray.length;
	}
	
	public int nearestNodeIndex(int hubIndex) 
	{
		int nodeIndex = -1;
		float nearestDistance = Float.MAX_VALUE;
		for(int i = 0; i < Solution.allDistances[hubIndex].length; i++)
		{
			if(i != this.hubsIndex[hubIndex] && this.nodeArray[i].isFree() && ! this.nodeArray[i].isHub() 
					&& Solution.allDistances[hubIndex][i] < nearestDistance)
			{
				nearestDistance = Solution.allDistances[hubIndex][i];
				nodeIndex = i;
			}
		}
		return nodeIndex;
	}
	
	public int getIndexMaxDistanceAllDistances() 
	{
		int nodeIndex = -1;
		float maxDistance = 0.0f;
		for(int i = 0; i < Solution.allDistances.length; i++)
		{
			for(int j = i; j < Solution.allDistances[i].length; j++)
			{
				if(i != j && (this.nodeArray[i].isFree() || this.nodeArray[j].isFree()) 
						&& (! this.nodeArray[i].isHub() || ! this.nodeArray[j].isHub()) 
						&& Solution.allDistances[j][i] > maxDistance)
				{
					maxDistance = Solution.allDistances[j][i];
					if(this.nodeArray[i].isHub())
						nodeIndex = j;
					else
						nodeIndex = i;
				}
			}
		}
		return nodeIndex;
	}
	
	public int getIndexMaxDistanceAllDistances(List<Integer> exceptions) 
	{
		int nodeIndex = -1;
		float maxDistance = 0.0f;
		for(int i = 0; i < Solution.allDistances.length; i++)
		{
			if(exceptions.contains(i))
			{
				continue;
			}
			for(int j = i; j < Solution.allDistances[i].length; j++)
			{
				if(! exceptions.contains(j) && 
						i != j && (this.nodeArray[i].isFree() || this.nodeArray[j].isFree()) 
						&& (! this.nodeArray[i].isHub() || ! this.nodeArray[j].isHub()) 
						&& Solution.allDistances[j][i] > maxDistance)
				{
					maxDistance = Solution.allDistances[j][i];
					if(this.nodeArray[i].isHub())
						nodeIndex = j;
					else
						nodeIndex = i;
				}
			}
		}
		return nodeIndex;
	}
	
	public void changeHub(int hubIndex, int indexNetList) 
	{
		List<Node> nodeList = this.hubNetList[hubIndex];
		int idNode = nodeList.get(indexNetList).getId();
		
		this.nodeArray[this.hubsIndex[hubIndex]].setHub(false);
		this.hubsIndex[hubIndex] = idNode - 1;
		this.nodeArray[idNode - 1].setHub(true);
		
		int idNewHub = this.nodeArray[idNode - 1].getId();
		Node node = null;
		for(int i = 0; i < nodeList.size(); i++)
		{
			if(nodeList.get(i).getId() == idNewHub)
			{
				node = nodeList.remove(i);
				break;
			}
		}
		Node oldHub = nodeList.remove(0);
		nodeList.add(0, node);
		nodeList.add(nodeList.size(), oldHub);
		this.updateDistances(hubIndex);
	}
	
	private void updateDistances(int hubIndex) 
	{
		float distance = 0.0f;
		
		for(Node node : this.hubNetList[hubIndex])
		{
			distance += Solution.allDistances[this.hubsIndex[hubIndex]][node.getId() - 1];
		}
		this.distances[hubIndex] = distance;
	}

	public List<Node>[] getHubNetList() 
	{
		return this.hubNetList;
	}

	public void setHubNetList(List<Node>[] hubNetList) 
	{
		this.hubNetList = hubNetList;
	}

	public float[] getDistances() 
	{
		return this.distances;
	}

	public void setDistances(float[] distances) 
	{
		this.distances = distances;
	}
	
	public Node[] getNodeArray() 
	{
		return this.nodeArray;
	}

	public void setNodeArray(Node[] nodeArray)
	{
		this.nodeArray = nodeArray;
	}

	public int[] getHubsIndex()
	{
		return this.hubsIndex;
	}

	public void setHubsIndex(int[] hubsIndex) 
	{
		this.hubsIndex = hubsIndex;
	}

	public int getHubsAdded() 
	{
		return this.hubsAdded;
	}

	public static float[][] getAllDistances() 
	{
		return Solution.allDistances;
	}

	public static void setAllDistances(float[][] allDistances) 
	{
		Solution.allDistances = allDistances;
	}

	// Objective function
	public float getDistance()
	{
		float totalDistance = 0.0f;
		for(float distance : this.distances)
		{
			totalDistance += distance;
		}
		return totalDistance;
	}
	
	public float getMaxDistance()
	{
		float max = 0.0f;
		for(int i = 0; i < this.distances.length; i++)
		{
			max = max < this.distances[i] ? this.distances[i] : max; 
		}
		return max;
	}
	
	public float getMinDistance()
	{
		float min = Float.MAX_VALUE;
		for(int i = 0; i < this.distances.length; i++)
		{
			min = min > this.distances[i] ? this.distances[i] : min; 
		}
		return min;
	}
	
	public int getIndexMaxDistance()
	{
		float max = 0.0f;
		int index = 0;
		for(int i = 0; i < this.distances.length; i++)
		{
			if(max < this.distances[i])
			{
				max = this.distances[i];
				index = i;
			}
		}
		return index;
	}
	
	public int getIndexMinDistance()
	{
		float min = Float.MAX_VALUE;
		int index = 0;
		for(int i = 0; i < this.distances.length; i++)
		{
			if(min > this.distances[i])
			{
				min = this.distances[i];
				index = i;
			}
		}
		return index;
	}

	@Override
	public int compareTo(Solution s)
	{
		float d = this.getDistance();
		float ds = s.getDistance();
		return d == ds ? 0 : d > ds ? 1 : -1;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder("[");
		for(List<Node> listNode : this.hubNetList)
		{
			sb.append(listNode);
		}
		sb.append("]");
		return sb.toString();
	}

}
