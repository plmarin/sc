package compute;

import java.util.List;
import java.util.Random;

import reading.FileRead;
import solution.Solution;

/*
 * This class compute a solution from giving solution
 * through some metaheuristics (stipulated in the Algorithm class)
 */
public class MetaheuristicAlgorithm implements ComputeAlgorithm
{
	private final long TOTAL_TIME;
	private static final int TOTAL_ITERATIONS;
	private final long INITIAL_TIME;
	private final FileRead FILE_READ;
	private Solution solution;
	private Algorithm algorithm;
	
	
	static
	{
		TOTAL_ITERATIONS = 10; // KMAX
	}

	public MetaheuristicAlgorithm(long time, LocalSearchAlgorithm localSearchAlgorithm, long totalTime) 
	{
		this(time,
			localSearchAlgorithm.getFileRead(),
			localSearchAlgorithm.getSolution(),
			totalTime);
	}
	
	public MetaheuristicAlgorithm(long time, FileRead fileRead, Solution solution, long totalTime) 
	{
		this.INITIAL_TIME = time;
		this.FILE_READ = fileRead;
		this.solution = new Solution(solution);
		this.TOTAL_TIME = totalTime;
	}
	
	// Compute Algorithm
	@Override
	public void compute(Algorithm algorithm)
	{
		this.algorithm = algorithm;
		if(this.algorithm.name().equalsIgnoreCase(Algorithm.ITERATED_GREEDY.name()))
		{
			this.iteratedGreedy();
		}
		else if(this.algorithm.name().equalsIgnoreCase(Algorithm.BASIC_VARIABLE_NEIGHBOURHOOD_SEARCH.name()))
		{
			this.basicVariableNeighbourhoodSearch(); 
		}
		else
		{
			System.err.println(algorithm.name() + " algorithm hasn't implement yet." +
					" Try with other constructive algorithm");
			System.exit(-1);
		}
	}

	/*
	 * BASIC VARIABLE NEIGHBOURHOOD SEARCH
	 * 
	 * 1. procedimiento BVNS(f, kmax, tmax)
	 * 2. 	repetir
	 * 3. 		k <-- 1;
	 * 4.		repetir
	 * 5.			f' <-- Shake(f, k);  // Randomization
	 * 6.			f'' <-- LocalSearch(f', k);
	 * 7.			NeighbourhoodChange(f, f'', k);
	 * 8.		hasta k = kmax;
	 * 9.		t <-- CPUTime();
	 * 10.	hasta t > tmax;
	 * 11.fin BVNS;
	 */
	private void basicVariableNeighbourhoodSearch() 
	{
		int k = 1;
		Solution s;
		LocalSearchAlgorithm lsa = new LocalSearchAlgorithm(this.FILE_READ);
		do
		{
			s = this.shake(k);
			lsa.setSolution(s);
			lsa.compute(Algorithm.FIRST_RANDOM_IMPROVEMENT);
			s = lsa.getSolution();
			k = this.neighbourhoodChange(s, k);
		}while(! this.stopCondition(k));
	}
	
	// Solution randomization
	private Solution shake(int k) 
	{
		Solution s = new Solution(this.solution);
		Random rnd = new Random(k);
		List<Integer> indexList = s.getIndexList();
		int element;
		int removePos;
		int addPos;
		int minPos = indexList.size();
		int maxPos = 0;
		for(int i = 0; i < k; i++)
		{
			removePos = rnd.nextInt(indexList.size());
			element = indexList.remove(removePos);
			addPos = rnd.nextInt(indexList.size());
			indexList.add(addPos, element);
			if(removePos > addPos)
			{
				if(maxPos < removePos)
				{
					maxPos = removePos;
				}
				if(minPos > addPos)
				{
					minPos = addPos;
				}
			}
			else
			{
				if(maxPos < addPos)
				{
					maxPos = addPos;
				}
				if(minPos > removePos)
				{
					minPos = removePos;
				}
			}
		}
		s.computeCuts(minPos, maxPos);
		return s;
	}

	/*
	 * 1. procedimiento NeighbourhoodChange(x, x', k)
	 * 2.	si (f(x') < f(x)) entonces
	 * 3.		x <-- x';
	 * 4.		k <-- 1;
	 * 5.	si no (k <-- k + 1);
	 * 6. fin NeighbourhoodChange;
	 */
	private int neighbourhoodChange(Solution s, int k)
	{
		int aux;
		if(s.maxCut() < this.solution.maxCut())
		{
			this.solution = new Solution(s);
			aux = 1;
		}
		else
		{
			aux = ++k;
		}
		return aux;
	}

	/*
	 * ITERATED GREEDY
	 * 
	 * 1. procedimiento IG
	 * 2. 	x <-- ConstruirSolucionInicial();
	 * 3. 	x' <-- BusquedaLocal(x);
	 * 4.	mientras (not CriterioParada) hacer
	 * 5.		solParcial <-- DestruirSolucion(x');
	 * 6.		x'' <-- ConstruirSolucion(solParcial);
	 * 7.		x' <-- CriterioAceptacion(x'', x');
	 * 8.	fin mientras;
	 * 9.	devolver x';
	 * 10. fin IG;
	 */
	private void iteratedGreedy() 
	{
		System.err.println(Algorithm.ITERATED_GREEDY.name() + " algorithm hasn't implement yet." +
				" Try with other metaheuristic algorithm");
		System.exit(-1);
	}

	private boolean stopCondition(int iterations) 
	{
		boolean stopCondition;

		// Time
		stopCondition = System.currentTimeMillis() - this.INITIAL_TIME > this.TOTAL_TIME;
		// Iterations
		stopCondition = stopCondition || iterations > MetaheuristicAlgorithm.TOTAL_ITERATIONS;

		return stopCondition;
	}

	public FileRead getFileRead() 
	{
		return this.FILE_READ;
	}
	
	@Override
	public Solution getSolution() 
	{
		return this.solution;
	}
	
	@Override
	public String toString() 
	{
		StringBuilder sb = new StringBuilder("[absolutePathFileRead= \"" + this.FILE_READ.getAbosultePathFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("fileRead= \"" + this.FILE_READ.getFileName());
		sb.append("\", " + System.lineSeparator());
		sb.append("solution= " + this.solution);
		sb.append("," + System.lineSeparator());	
		sb.append("Objetive Function= " + this.solution.maxCut() + "]");
		return sb.toString();
	}

}
